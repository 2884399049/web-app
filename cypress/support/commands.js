import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'
import 'firebase/firestore'
import { attachCustomCommands } from 'cypress-firebase'

const fbConfig = {
  apiKey: "AIzaSyCsZ4aXGN1hW9Suwbb6GbaTx7cn9wpCDS0",
  authDomain: "hc-application-interface-dev.firebaseapp.com",
  databaseURL: "https://hc-application-interface-dev.firebaseio.com",
  projectId: "hc-application-interface-dev",
  storageBucket: "hc-application-interface-dev.appspot.com",
  messagingSenderId: "952852633903",
  appId: "1:952852633903:web:375d2c61fe6107e5"
}

window.fbInstance = firebase.initializeApp(fbConfig)

attachCustomCommands({ Cypress, cy, firebase })
