import Vue from 'vue'

import { BootstrapVue } from 'bootstrap-vue'
import VueFormWizard from 'vue-form-wizard'
import datePicker from 'vue-bootstrap-datetimepicker'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faClock, faCalendar, faArrowUp, faArrowDown, faChevronLeft, faChevronRight, faCalendarCheck, faTrashAlt, faTimesCircle } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import jQuery from 'jquery'
import Vue2Filters from 'vue2-filters'
import VueLodash from 'vue-lodash'

import VuePhoneNumberInput from 'vue-phone-number-input'
import AsyncComputed from 'vue-async-computed'

import 'vue-phone-number-input/dist/vue-phone-number-input.css'
import 'pc-bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.css'
import 'vue-form-wizard/dist/vue-form-wizard.min.css'
import '@voerro/vue-tagsinput/dist/style.css'
import 'bootstrap/dist/css/bootstrap.css'
import '@/assets/scss/custom/components/_customWizard.scss'
import '@/assets/custom.scss'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import 'prismjs'
import 'prismjs/themes/prism.css'

import * as constants from '../../functions/shared/constants.js'
import config from '@/lib/config.js'
import text_utils from '@/lib/text_utils.js'
import moment from 'moment'

let { DELAY_DELIVERY_DAYS, BATCH_STATUS, MIN_DELIVERY_DATE, PRODUCT, MIN_UNITS, MAX_UNITS, CAMPAIGN_STATUS, RECIPIENT_STATUS, PAPER_SIZES, ENVELOPE_SIZES, HANDWRITING_SIZE, HANDWRITING_STYLE, HEADER_TYPE, STAMP_TYPES, DATA_SOURCE, SENDER, ENVELOPE_REQUIRED, FONTS, GOOD } = constants

library.add(faClock)
library.add(faCalendar)
library.add(faArrowUp)
library.add(faArrowDown)
library.add(faChevronLeft)
library.add(faChevronRight)
library.add(faCalendarCheck)
library.add(faTrashAlt)
library.add(faTimesCircle)

Vue.mixin({
  data: function () {
    return {
      CAMPAIGN_STATUS: CAMPAIGN_STATUS,
      RECIPIENT_STATUS: RECIPIENT_STATUS,
      PAPER_SIZES: PAPER_SIZES,
      ENVELOPE_SIZES: ENVELOPE_SIZES,
      HANDWRITING_SIZE: HANDWRITING_SIZE,
      HANDWRITING_STYLE: HANDWRITING_STYLE,
      HEADER_TYPE: HEADER_TYPE,
      STAMP_TYPES: STAMP_TYPES,
      DATA_SOURCE: DATA_SOURCE,
      SENDER: SENDER,
      DELAY_DELIVERY_DAYS: DELAY_DELIVERY_DAYS,
      ENVELOPE_REQUIRED: ENVELOPE_REQUIRED,
      FONTS: FONTS,
      MIN_UNITS: MIN_UNITS,
      MAX_UNITS: MAX_UNITS,
      PRODUCT: PRODUCT,
      MIN_DELIVERY_DATE: MIN_DELIVERY_DATE,
      CONFIG: config,
      BATCH_STATUS: BATCH_STATUS,
      GOOD: GOOD,
      CONSTANTS: constants
    }
  },
  filters: {
    to_title_case: text_utils.to_title_case
  },
  methods: {
    display_timestamp: function (timestamp, date_only, show_seconds) {
      if (!timestamp) {
        return
      }

      if (typeof timestamp === 'string') {
        try {
          let out = timestamp
          if (date_only) {
            out = moment(timestamp).format('L')
          } else if (show_seconds) {
            out = moment(timestamp).format('HH:mm:ss L')
          } else {
            out = moment(timestamp).format('HH:mm L')
          }

          if (out === 'Invalid date') {
            return timestamp
          } else {
            return out
          }
        } catch (err) {
          return timestamp
        }
      }

      try {
        if (date_only) {
          return moment(timestamp.toDate()).format('L')
        } else if (show_seconds) {
          return moment(timestamp.toDate()).format('HH:mm:ss L')
        } else {
          return moment(timestamp.toDate()).format('HH:mm L')
        }
      } catch (err) {
        return ''
      }
    }
  }
})

Vue.config.productionTip = false

Vue.component('vue-phone-number-input', VuePhoneNumberInput)
Vue.use(Vue2Filters)
Vue.use(BootstrapVue)
Vue.use(VueFormWizard)
Vue.use(datePicker)
Vue.use(VueLodash)
Vue.use(AsyncComputed)
Vue.component('font-awesome-icon', FontAwesomeIcon)

jQuery.extend(true, jQuery.fn.datetimepicker.defaults, {
  icons: {
    time: 'far fa-clock',
    date: 'far fa-calendar',
    up: 'fas fa-arrow-up',
    down: 'fas fa-arrow-down',
    previous: 'fas fa-chevron-left',
    next: 'fas fa-chevron-right',
    today: 'fas fa-calendar-check',
    clear: 'far fa-trash-alt',
    close: 'far fa-times-circle'
  }
})
