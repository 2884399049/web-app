import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/storage'
import 'firebase/functions'
import 'firebase/auth'
import config from '@/lib/config.js'

firebase.initializeApp(config.firebase_config)

let db = firebase.firestore()
let storage = firebase.storage()
let functions = firebase.functions()
let auth = firebase.auth()
let perf = {}
let FieldValue = firebase.firestore.FieldValue
// if (process.env.NODE_ENV !== 'test') {
//   // Broken?
//   // require('firebase/performance')
//   // perf = firebase.performance()
// }
let serverTimestamp = firebase.firestore.FieldValue.serverTimestamp
let firestoreDocumentID = firebase.firestore.FieldValue.documentId
let fb = firebase

// https://firebase.google.com/docs/emulator-suite/connect_and_prototype?database=Firestore#web
if (process.env.NODE_ENV === 'local' || process.env.NODE_ENV === 'local-firestore' || process.env.NODE_ENV === 'test') {
  functions.useFunctionsEmulator('http://0.0.0.0:5000')
  console.warn('Using firebase functions emulator.')
}

if (process.env.NODE_ENV === 'local-firestore' || process.env.NODE_ENV === 'test') {
  db.settings({host: '0.0.0.0:8080', ssl: false})
  console.warn('Using firebase firestore emulator.')
}

export {
  db, storage, fb, functions, auth, perf, serverTimestamp, firestoreDocumentID, config, FieldValue
}
