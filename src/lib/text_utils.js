const _ = require('underscore')
const countriesList = require('./countries')

var ILLEGAL_CHARACTERS = {
  '’': "'",
  '‘': "'",
  '،': ',',
  '‐': '-',
  '‒': '-',
  '–': '-',
  '—': '-',
  '―': '-',
  '一': '-',
  '-': '-',
  '“': '"',
  '”': '"',
  '/': '/',
  '⧸': '/',
  '⁄': '/',
  ' ': ' ',
  ' ': ' ',
  '\xc2\xa0': ' ',
  '\xe2\x80\x88': ' ',
  '\xe2\x80\x8B': ' ',
  '…': '...',
  '′': "'",
  '″': "''",
  '‴': "'''"
}

var TEXT_TEMPLATES = {
  fundraising: `Dear {{{first name}}}\n\nWe are looking to raise (amount) but we can't do it without generous donations from companies such as yours!\n\nBy sponsoring us, you will receive (offer incentive), and with your help, we hope to make this year one of our best yet. Thank you for your time and we hope to hear from you soon.\n\nSincerely, (organisation)`,
  event_invitation: `Dear {{{first name}}}\n\n(The event) will be taking place on (date) at (location) and we would love for you to attend!\n\nCome enjoy a (day/evening) of (what can be expected) and make some memories you won't forget. Contact (number) to RSVP. We look forward to seeing you there!\n\nSincerely,\n     (organisation)`,
  customer_service_apology: `Dear {{{first name}}}\n\nWe would like to apologise for (reason). This is not typically the way that we like to do business and you deserve much better from us.\n\nOur customers are very important to us and we deeply care about your experiences.\nIf there is anything we can do to make it up to you please let us know at (number)).\n\nSincerely, (organisation)`,
  birthday: `Dear {{{first name}}},\n\nWe would just like to say: Happy Birthday!\n\nThis special day should be all about you and we hope that it brings you many happy memories. We'll be thinking of you today, wishing you the very best.\n\nLots of love,\n     All of us at (organisation)`,
  new_product: `Dear {{{first name}}}\n\nWe'd like to say thank you for staying with us all this time! To show you just how much you mean to us, here is (an exclusive early look at our new product/a discount). We think you'll love what we have to offer in the future, so be sure to keep sticking with us.\n\nYours,\n     (organisation)`,
  customer_loyalty: `Dear {{{first name}}}\n\nThis is a letter to let you know that we really appreciate having you as a customer. We hope you love what we have to offer, and if there is anything that would help improve your experience, then please let us know. We are always looking for new to please our customers.\n\nKindest regards,\n     (organisation)`,
  customer_re_engagement: `Dear {{{first name}}}\n\nWe haven't heard from you for a while. A lot has changed since you've been gone and we've got a lot of new stuff that we think you'd love to see. Here's (discount/other incentive) on us, so please get in touch and let us know you're alright!\n\nAll the best,\n     (organisation)`,
  lead_generation: `Dear {{{first name}}}\n\n(Open with what your company does)\n\nWe think you'll love what we have to offer.\n\nPlease feel free to reach out to us to see just what we can do to help your business. We'd love to give you a free consultation.\n\nKind regards,\n     (organisation)`,
  meeting_follow_up: `Dear {{{first name}}}\n\nThank you for taking the time to meet with us. It was great to finally speak with you in person and we appreciate the ideas that you brought to the table. We hope that you enjoyed the discussion just as much as we did and if there is anything else we can do for you please don't hesitate to ask.\n\nKind regards, (organisation)`
}

function remove_illigal_characters (text) {
  var clean_text = ''

  if (!text) {
    return clean_text
  }

  for (var i in text) {
    if (ILLEGAL_CHARACTERS[text[i]] !== undefined) {
      clean_text = clean_text + ILLEGAL_CHARACTERS[text[i]]
    } else {
      clean_text = clean_text + text[i]
    }
  }

  // Remove multiple spaces, leading spaces
  // clean_text = clean_text.replace(/ +(?= )/g, '')
  clean_text = clean_text.replace(/ ,/g, ',')
  clean_text = clean_text.replace(/ \./g, '.')
  clean_text = clean_text.replace(/,+/g, ',')
  clean_text = clean_text.replace(/\r\n|\r|\n/g, '\n')

  return clean_text
}

function to_title_case (str) {
  let text = str.replace(
    /\w\S*/g,
    function (txt) {
      // return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase()
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase()
    }
  )

  // text.replace(/(?<=-).\w*/g, txt => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase())
  var regExp = new RegExp(/-(\w+)/g)
  if (regExp.test(text)) {
    return text.split('-')
      .map(txt => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase())
      .join('-')
  }

  return text
}

function template_text (template, variables, include_country_name) {
  if (variables === undefined) {
    variables = {
      'title': 'Mr',
      'first name': 'John',
      'last name': 'Doe',
      'department': 'Marketing',
      'company': 'Company',
      'address line 1': '1 Alfred Place',
      'address line 2': 'Fitzrovia',
      'address line 3': 'Address line 3',
      'city': 'London',
      'region': 'London',
      'postal_code': 'TW11 9PB',
      'country': 'United Kingdom',
      'custom 1': 'Custom 1',
      'custom 2': 'Custom 2',
      'custom 3': 'Custom 3',
      'custom 4': 'Custom 4',
      'custom 5': 'Custom 5',
      'custom 6': 'Custom 6'
    }
  }

  var address = ''
  if (variables['title']) address += `${variables['title']} `
  if (variables['first name']) address += `${to_title_case(variables['first name']).trim()} `
  if (variables['last name']) address += `${to_title_case(variables['last name']).trim()}\n`
  if (variables['department']) address += `${variables['department']}\n`
  if (variables['company']) address += `${variables['company']}\n`
  if (variables['address line 1']) address += `${to_title_case(variables['address line 1']).trim()}\n`
  if (variables['address line 2']) address += `${to_title_case(variables['address line 2']).trim()}\n`
  if (variables['address line 3']) address += `${to_title_case(variables['address line 3']).trim()}\n`

  let postal_code = variables['zip/postal code'] || variables['postal_code']
  let region = variables['state/region'] || variables['region']

  if (variables['country'] && ['US'].indexOf(variables['country']) > -1) {
    if (variables['city'] && region) {
      address += `${to_title_case(variables['city']).trim()} ${region}`
      if (postal_code) address += ` ${postal_code}\n`
    } else if (variables['city']) {
      address += `${to_title_case(variables['city']).trim()}`
      if (postal_code) address += ` ${postal_code}\n`
    } else if (region) {
      address += `${region}`
      if (postal_code) address += ` ${postal_code}\n`
    } else {
      if (postal_code) address += `${postal_code}\n`
    }
  } else {
    if (variables['city'] && region) {
      address += `${to_title_case(variables['city']).trim()}, ${region}\n`
    } else if (variables['city']) {
      address += `${to_title_case(variables['city']).trim()}\n`
    } else if (region) {
      address += `${region}\n`
    }

    if (postal_code) address += `${postal_code}\n`
  }

  // Only include country if not GB or US
  if (variables['country'] && ((include_country_name || ['GB'].indexOf(variables['country']) === -1) && ['US'].indexOf(variables['country']) === -1)) {
    if (variables['country'] && countriesList[variables['country'].toUpperCase()]) {
      address += `${countriesList[variables['country'].toUpperCase()]}\n`
    } else if (variables['country']) {
      address += `${variables['country']}\n`
    }
  }

  variables['address'] = address.trim()

  for (var key in variables) {
    if (variables[key]) {
      template = template.replace(new RegExp(`{{{${key}}}}`, 'g'), variables[key])
    }
  }

  template = template.replace(/{{{.+}}}/g, '') // Clean any missing

  return template
}

function textarea_variable_highlight (text) {
  let txt = text
    .replace(/\n$/g, '\n\n')
    .replace(
      /{{{first name}}}|{{{last name}}}|{{{department}}}|{{{title}}}|{{{company}}}|{{{city}}}|{{{custom 1}}}|{{{custom 2}}}/gm,
      x => `<mark class="highlight">${x.substr(0, 3) + x.charAt(3).toUpperCase() + x.substr(4)}</mark>`
    )
    .replace(/{{{|}}}/g, '   ')

  return txt
}

export default {
  TEXT_TEMPLATES: TEXT_TEMPLATES,
  get_variables (text) {
    let groups = text.match(/{{{(.+?)}}}/g)
    let variables = []
    if (groups) {
      for (let variable of groups) {
        variables.push(variable.replace(/{{{/g, '').replace(/}}}/g, ''))
      }
    }
    return variables
  },
  replace_unsupported_chars (text) {
    let new_text = text

    new_text = new_text.replace(/[ăàáâäãåāªà]/g, 'a')
    new_text = new_text.replace(/[ß]/g, 'ß')
    new_text = new_text.replace(/[æ]/g, 'ae')
    new_text = new_text.replace(/[œ]/g, 'oe')
    new_text = new_text.replace(/[сçćč]/g, 'c')
    new_text = new_text.replace(/[еèéêëēėę]/g, 'e')
    new_text = new_text.replace(/[ıîïíīįì]/g, 'i')
    new_text = new_text.replace(/[ł]/g, 'l')
    new_text = new_text.replace(/[ñń]/g, 'n')
    new_text = new_text.replace(/[оðôöòóøōõº]/g, 'o')
    new_text = new_text.replace(/[р]/g, 'p')
    new_text = new_text.replace(/[şßśš]/g, 's')
    new_text = new_text.replace(/[ûüùúū]/g, 'u')
    new_text = new_text.replace(/[ÿ]/g, 'y')
    new_text = new_text.replace(/[žźż]/g, 'z')

    new_text = new_text.replace(/[АÀÁÄÂÃÅĀ]/g, 'A')
    new_text = new_text.replace(/[Æ]/g, 'AE')
    new_text = new_text.replace(/[Œ]/g, 'OE')
    new_text = new_text.replace(/[ÇĆČ]/g, 'C')
    new_text = new_text.replace(/[ÈÉÊËĒĖĘ]/g, 'E')
    new_text = new_text.replace(/[н]/g, 'H')
    new_text = new_text.replace(/[İÎÏÍĪĮÌ]/g, 'I')
    new_text = new_text.replace(/[Ł]/g, 'L')
    new_text = new_text.replace(/[иÑŃ]/g, 'N')
    new_text = new_text.replace(/[ÔÖÒÓØŌÕ]/g, 'O')
    new_text = new_text.replace(/[ŞŚŠ]/g, 'S')
    new_text = new_text.replace(/[т]/g, 'T')
    new_text = new_text.replace(/[ÛÜÙÚŪ]/g, 'U')
    new_text = new_text.replace(/[Ÿ]/g, 'Y')
    new_text = new_text.replace(/[ŽŹŻ]/g, 'Z')

    new_text = new_text.replace(/\[/g, '(')
    new_text = new_text.replace(/\]/g, ')')

    return new_text
  },
  check_bad_text (text, that, prepend, show_toast) {
    if (!prepend) {
      prepend = ''
    }

    var new_text = remove_illigal_characters(text)
    var search_text = new_text.replace(/{{{.+}}}/g, '')
    let bad_chars = []
    /* eslint-disable-next-line */
    if (!/^[\x00-\x7F]*$/.test(search_text) || /[\{\}\[\]\^\\_<>\`]/.test(search_text)) {
      // eslint-disable-next-line
      bad_chars = _.uniq(search_text.replace(/[a-zA-Z0-9!@$%&*()-+=.,:'"/\\n\\r|`]/gm, '').trim().split(''))
      // console.log(bad_chars)

      let bad_chars_str = ''
      for (var i in bad_chars) {
        bad_chars_str += `"${bad_chars[i]}"`
      }

      if (show_toast) {
        console.log('show toast..')
        that.$bvToast.toast(`${prepend}Character currently not supported. Please remove characters: ${bad_chars_str.split('""').join('", "')}.`, {
          title: `Unsupported character`,
          autoHideDelay: 5000,
          variant: 'danger',
          appendToast: true
          // static: true
        })
      }

      // that.$swal('Unsupported character', `${prepend}Character currently not supported. Please remove characters: ${bad_chars.join(', ')}.`, 'warning')
      text = new_text
      return [false, new_text, bad_chars]
    }

    text = new_text
    return [true, new_text, bad_chars]
  },
  to_title_case,
  template_text,
  textarea_variable_highlight
}
