import Vue from 'vue'
import Router from 'vue-router'
import StationaryList from '@/views/StationaryList'
import NewCampaign from '@/views/NewCampaign'
import EditStationery from '@/views/EditStationery'
import Account from '@/views/Account'
import CampaignList from '@/views/CampaignList'
import Campaign from '@/views/Campaign'
import Onboarding from '@/views/Onboarding'
import BatchList from '@/views/BatchList'
import Batch from '@/views/Batch'
import UserList from '@/views/UserList'
import User from '@/views/User'
import PrinterList from '@/views/PrinterList'
import Printer from '@/views/Printer'
import Analytics from '@/views/analytics/Analytics'
import Team from '@/pages/Team'
import Invoices from '@/pages/Invoices'
import Referral from '@/pages/Referral'
import Settings from '@/pages/Settings'
import Integrations from '@/pages/Integrations'
import Dashboard from '@/pages/Dashboard'
import Support from '@/pages/Support'
import Billing from '@/pages/Billing'
import Campaigns from '@/pages/Campaigns'
import CreateNew from '@/pages/CreateNew'
import CreateCover from '@/pages/CreateCover'

import Audience from '@/pages/Audience'
import AudienceDetail from '@/pages/AudienceDetail'
import Calendar from '@/pages/Calendar'

// import OnboardingOrganisation from '@/components/OnboardingOrganisation'
import OnboardingQuestions from '@/components/OnboardingQuestions'

Vue.use(Router)

// https://stackoverflow.com/questions/51980296/detect-back-button-in-navigation-guards-of-vue-router
// window.popStateDetected = false
// window.addEventListener('popstate', () => {
//   window.popStateDetected = true
// })

var router = new Router({
  mode: 'history',
  routes: [
    { path: '/', name: 'Dashboard', component: Dashboard, props: true, meta: { title: `Stationery | Scribeless` } },
    { path: '/stationery', name: 'Stationery', component: StationaryList, props: true, meta: { title: `Stationery | Scribeless` } },
    { path: '/edit/stationery/:id', name: 'EditStationery', component: EditStationery, props: true, meta: { title: `Edit Stationery | Scribeless` } },
    { path: '/stationery/:id', alias: '/stationery/:id/*', name: 'NewCampaign', component: NewCampaign, props: true, meta: { title: `Stationery | Scribeless` } },
    { path: '/stationery/:id', alias: '/stationery/:id/*', name: 'CreateNew', component: CreateNew, props: true, meta: { title: `Stationery | Scribeless` } },

    { path: '/audience/:id', alias: '/audience/:id/*', name: 'AudienceDetail', component: AudienceDetail, props: true, meta: { title: `Audience | Scribeless` } },
    { path: '/team', name: 'Team', component: Team, meta: { title: `Account | Scribeless` } },
    { path: '/settings', name: 'Settings', component: Settings, meta: { title: `Settings | Scribeless` } },
    { path: '/integration', name: 'Integrations', component: Integrations, meta: { title: `Integrations | Scribeless` } },
    { path: '/referral', name: 'Referral', component: Referral, meta: { title: `Referral` } },
    { path: '/invoice', name: 'Invoices', component: Invoices, meta: { title: `Invoices` } },
    { path: '/audience', name: 'Audience', component: Audience, meta: { title: `Account | Scribeless` } },
    { path: '/support', name: 'Support', component: Support, meta: { title: `Support | Scribeless` } },
    { path: '/billing', name: 'Billing', component: Billing, meta: { title: `Billing | Scribeless` } },
    { path: '/scheduled-campaigns', name: 'Calendar', component: Calendar, meta: { title: `Calendar | Scribeless` } },

    { path: '/create', name: 'Create', component: CreateNew, meta: { title: `Billing | Scribeless` } },
    { path: '/createcover', name: 'Cover', component: CreateCover, meta: { title: `Create | Scribeless` } },

    { path: '/account', name: 'Account', component: Account, meta: { title: `Account | Scribeless` } },
    {
      path: '/onboarding',
      name: 'Onboarding',
      component: Onboarding,
      meta: { title: `Onboarding | Scribeless`, hide_navbar: true },
      children: [
        // { path: 'organisation', name: 'OnboardingOrganisation', component: OnboardingOrganisation, meta: {title: `Onboarding | Scribeless`, hide_navbar: true} },
        { path: 'questions', name: 'OnboardingQuestions', component: OnboardingQuestions, meta: { title: `Onboarding | Scribeless`, hide_navbar: true } }
      ]
    },
    { path: '/campaigns', name: 'CampaignList', component: Campaigns, meta: { title: `Campaigns | Scribeless` } },
    { path: '/campaign/:id', name: 'Campaign', component: Campaign, meta: { title: `Campaign | Scribeless` } },
    { path: '/batches', name: 'BatchList', component: BatchList, meta: { title: `Batches | Scribeless` } },
    { path: '/users', name: 'UserList', component: UserList, meta: { title: `Users | Scribeless` } },
    { path: '/analytics', name: 'Analytics', component: Analytics, meta: { title: `Analytics | Scribeless` } },
    { path: '/printers', name: 'PrinterList', component: PrinterList, meta: { title: `Printers | Scribeless` } },
    { path: '/printer/:id', name: 'Printer', component: Printer, meta: { title: `Printer | Scribeless` } },
    { path: '/user/:id', name: 'User', component: User, meta: { title: `User | Scribeless` } },
    { path: '/batch/:id', name: 'Batch', component: Batch, meta: { title: `Batch | Scribeless` } },
    { path: '/logout', name: 'Logout', component: Account, meta: { title: `Logout | Scribeless` } },
    { path: '*', redirect: { name: 'Stationery' } }
  ]
})

router.beforeEach((to, from, next) => {
  // if (from.path.indexOf('edit') > -1 && from.path.indexOf('stationery') > -1) {
  //   // Pass
  // } else if (from.path.indexOf('stationery') > -1 && to.path.indexOf('stationery') === -1 && to.path.match(/campaign\/.+/) == null) {
  //   var result = confirm('You may lose unsaved work if you go back (to go back a stage click on the stage number). Are you sure you want to go back?')
  //   window.popStateDetected = false
  //   if (!result) {
  //     next(false)
  //     return ''
  //   }
  // }
  window.popStateDetected = false
  document.title = to.meta.title
  next()
})

export default router
