import { functions } from '@/lib/firebase.js'
import config from '@/lib/config.js'
import constants from '@/../functions/shared/constants.js'

function handle_error_response (swal, response) {
  if (!response.success) {
    console.warn(response)
    swal('Error', response.error_message, 'error')
    return true
  }

  return false
}

export default {
  handle_error_response,
  stripe_request_setupintent () {
    return functions.httpsCallable('stripe_request_setupintent')({})
  },
  stripe_add_card (payment_method_id, tax_id, tax_type) {
    return functions.httpsCallable('stripe_add_card')({payment_method_id, tax_id, tax_type})
  },
  stripe_add_coupon (coupon) {
    return functions.httpsCallable('stripe_add_coupon')({coupon})
  },
  rerender (recipient_id, campaign_id) {
    return functions.httpsCallable('rerender_recipient')({recipient_id, campaign_id})
  },
  add_recipients (recipients, campaign_id) {
    return functions.httpsCallable('add_recipients')({recipients, campaign_id, source: constants.RECIPIENT_SOURCE.interface})
  },
  interface_render_text (text, paper, text_size, handwriting_style, handwriting_colour, variables, _include_country_name) {
    /* eslint-disable */
    let include_country_name = _include_country_name !== undefined ? _include_country_name : false
    /* eslint-enable */
    return functions.httpsCallable('interface_render_text')({text, paper, text_size, handwriting_style, handwriting_colour, variables, include_country_name})
  },
  set_hubspot_connection_session (uid) {
    const options = {method: 'POST', body: JSON.stringify({uid: uid}), headers: {'Content-Type': 'application/json'}}

    return fetch(`${config.firebase_config.functions}/hubspot/auth`, options).then((response) => {
      return response.json()
    })
  },
  bill_user (uid) {
    return functions.httpsCallable('bill_user')({uid})
  },
  batch_printer_check (batch_id) {
    return functions.httpsCallable('batch_printer_check')({batch_id})
  },
  bill_users () {
    return functions.httpsCallable('bill_users')()
  },
  sync_hubspot_invoices () {
    return functions.httpsCallable('sync_hubspot_invoices')()
  }
}
