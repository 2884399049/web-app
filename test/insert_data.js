const projectId = 'route-v1-functions-test'
process.env.GCLOUD_PROJECT = projectId
process.env.FIRESTORE_EMULATOR_HOST = 'localhost:8080'

const admin = require('firebase-admin')
admin.initializeApp({ projectId })
const firestore = admin.firestore()

async function main() {
  await admin.firestore().collection('users').doc('1').set({admin: true})

  console.log('hello moto')
}

main()
