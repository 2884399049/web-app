const fs = require('fs')

const projectId = 'firestore-rules-test'
process.env.GCLOUD_PROJECT = projectId
process.env.FIRESTORE_EMULATOR_HOST = 'localhost:8080'

const firebase = require('@firebase/testing')

function path_to_ref(db, path) {
  let segments = path.split('/')
  let is_collection = true
  let ref = db
  for (let segment of segments) {
    if (is_collection) {
      ref = ref.collection(segment)
    } else {
      ref = ref.doc(segment)
    }
    is_collection = !is_collection
  }

  return ref
}

async function db_setup(auth, data) {
  let test_project_id = `${projectId}-${Date.now()}`

  const admin = await firebase.initializeAdminApp({projectId: test_project_id})
  const admin_db = admin.firestore()

  for (let path in data) {
    let doc = data[path]
    let ref = path_to_ref(admin_db, path)
    ref.set(doc)
  }

  const app = await firebase.initializeTestApp({projectId: test_project_id, auth: auth})
  const db = app.firestore()

  await firebase.loadFirestoreRules({projectId: test_project_id, rules: fs.readFileSync('firestore.rules', 'utf8')})

  return db
}

async function db_teardown() {
  return Promise.all(firebase.apps().map(app => app.delete()));
}

const mock_user_non_admin = {
  uid: 'test-1'
}

const mock_user_admin = {
  uid: 'test-2'
}

const mock_user_new = {
  uid: 'test-3'
}

const mock_data = {
  'users/test-1': {admin: false},
  'users/test-1/audit/1': {name: 'audit test-1 1'},
  'users/test-1/products/1': {name: 'product test-1 1'},
  'users/test-2': {admin: true},
  'users/test-2/audit/1': {name: 'audit test-2 1'},

  'campaigns/1': {name: 'Campaign 1', uid: mock_user_non_admin.uid},
  'campaigns/1/recipients/1': {name: 'Recipient 1'},
  'campaigns/2': {name: 'Campaign 2', uid: mock_user_admin.uid},
  'campaigns/2/recipients/1': {name: 'Recipient 1'},

  'usage/1': {name: 'Usage 1 (test-1)', uid: mock_user_non_admin.uid},
  'usage/2': {name: 'Usage 2 (test-2)', uid: mock_user_admin.uid},

  'stationary/1': {name: 'Stationary 1 (test-1)', uid: mock_user_non_admin.uid},
  'stationary/2': {name: 'Stationary 2 (test-2)', uid: mock_user_admin.uid},
  'stationary/3': {name: 'Stationary 1 (test-1)', uid: mock_user_admin.uid, template: true},

  'shopify/1': {name: 'Shopify 1 (test-1)', uid: mock_user_non_admin.uid},
  'shopify/2': {name: 'Shopify 2 (test-2)', uid: mock_user_admin.uid},

  'products/1': {name: 'Product 1'},

  'printers/1': {name: 'Printer 1'},

  'notifications/1': {name: 'Notification 1 (test-1)', uid: mock_user_non_admin.uid},
  'notifications/2': {name: 'Notification 2 (test-2)', uid: mock_user_admin.uid},

  'hubspot/1': {name: 'Hubspot 1 (test-1)', uid: mock_user_non_admin.uid},
  'hubspot/2': {name: 'Hubspot 2 (test-2)', uid: mock_user_admin.uid},

  'fireway/1': {name: 'fireway 1'},

  'invoices/1': {name: 'Invoice 1 (test-1)', uid: mock_user_non_admin.uid},
  'invoices/2': {name: 'Invoice 2 (test-2)', uid: mock_user_admin.uid},

  'batches/1': {name: 'Batch 1'},
  'batches/1/campaigns/1': {name: 'Batch 1 Campaign 1'},
  'batches/1/recipients/1': {name: 'Batch 1 Recipient 1'},

  'referral/1': {name: 'Referral 1'}
}

describe('admin user', () => {
  let db

  beforeAll(async () => {
    db = await db_setup(mock_user_admin, mock_data)
  })

  afterAll(async () => {
    await db_teardown()
  })

  it('Can read/edit all users', async () => {
    await firebase.assertSucceeds(db.collection('users').doc(mock_user_admin.uid).get())
    await firebase.assertSucceeds(db.collection('users').doc(mock_user_non_admin.uid).get())
    await firebase.assertSucceeds(db.collection('users').doc(mock_user_admin.uid).update({name: 'Ben', admin: true}))
    await firebase.assertSucceeds(db.collection('users').doc(mock_user_non_admin.uid).update({name: 'Ben', admin: true}))
  })

  it('Can read/edit all campaigns', async () => {
    await firebase.assertSucceeds(db.collection('campaigns').doc('1').get())
    await firebase.assertSucceeds(db.collection('campaigns').doc('2').get())
    await firebase.assertSucceeds(db.collection('campaigns').doc('1').update({x: '1'}))
    await firebase.assertSucceeds(db.collection('campaigns').doc('2').update({x: '1'}))
  })
})

describe('non admin user', () => {
  let db

  beforeAll(async () => {
    db = await db_setup(mock_user_non_admin, mock_data)
  })

  afterAll(async () => {
    await db_teardown()
  })

  it('Can read own user', async () => {
    await firebase.assertSucceeds(db.collection('users').doc(mock_user_non_admin.uid).get())
  })

  it('Can edit own user', async () => {
    await firebase.assertSucceeds(db.collection('users').doc(mock_user_non_admin.uid).update({name: 'Ben'}))
  })

  it('Cannot delete own user', async () => {
    await firebase.assertFails(db.collection('users').doc(mock_user_non_admin.uid).delete())
  })

  it('Cannot edit own user admin property', async () => {
    await firebase.assertFails(db.collection('users').doc(mock_user_non_admin.uid).update({admin: true}))
  })

  it('Cannot read/write/delete other users', async () => {
    await firebase.assertFails(db.collection('users').doc(mock_user_admin.uid).get())
    await firebase.assertFails(db.collection('users').doc(mock_user_admin.uid).update({name: 'Ben'}))
  })

  it('Can create an audit for own object', async () => {
    await firebase.assertSucceeds(db.collection('users').doc(mock_user_non_admin.uid).collection('audit').doc('2').set({x: 1}))
    await firebase.assertFails(db.collection('users').doc(mock_user_admin.uid).collection('audit').doc('2').set({x: 1}))
  })

  it('Can read a users product for own object', async () => {
    await firebase.assertSucceeds(db.collection('users').doc(mock_user_non_admin.uid).collection('products').doc('1').get())
    await firebase.assertFails(db.collection('users').doc(mock_user_non_admin.uid).collection('products').doc('2').set({x: 1}))
    await firebase.assertFails(db.collection('users').doc(mock_user_non_admin.uid).collection('products').doc('1').update({x: 1}))
    await firebase.assertFails(db.collection('users').doc(mock_user_non_admin.uid).collection('products').doc('1').delete())
  })

  it('Cannot read/update/delete audit', async () => {
    await firebase.assertFails(db.collection('users').doc(mock_user_non_admin.uid).collection('audit').doc('1').get())
    await firebase.assertFails(db.collection('users').doc(mock_user_non_admin.uid).collection('audit').doc('1').update({x: 1}))
    await firebase.assertFails(db.collection('users').doc(mock_user_non_admin.uid).collection('audit').doc('1').delete())
  })

  it('Can read/write own campaign but cannot delete', async () => {
    await firebase.assertSucceeds(db.collection('campaigns').doc('1').get())
    await firebase.assertSucceeds(db.collection('campaigns').doc('1').update({x: '1'}))
    await firebase.assertSucceeds(db.collection('campaigns').doc('3').set({x: '1'}))
    await firebase.assertFails(db.collection('campaigns').doc('1').delete())
  })

  it('Cannot read/update/delete other campaign', async () => {
    await firebase.assertFails(db.collection('campaigns').doc('2').get())
    await firebase.assertFails(db.collection('campaigns').doc('2').update({x: '1'}))
    await firebase.assertFails(db.collection('campaigns').doc('2').delete())
  })

  it('Can read/update/create own stationary but cannot delete', async () => {
    await firebase.assertSucceeds(db.collection('stationary').doc('1').get())
    await firebase.assertSucceeds(db.collection('stationary').doc('1').update({x: '1'}))
    await firebase.assertSucceeds(db.collection('stationary').doc('4').set({x: '1'}))
    await firebase.assertFails(db.collection('stationary').doc('1').delete())
  })

  it('Cannot read/update/delete other stationary', async () => {
    await firebase.assertFails(db.collection('stationary').doc('2').get())
    await firebase.assertFails(db.collection('stationary').doc('2').update({x: '1'}))
    await firebase.assertFails(db.collection('stationary').doc('2').delete())
  })

  it('Can read but not update/delete template stationary', async () => {
    await firebase.assertSucceeds(db.collection('stationary').doc('3').get())
    await firebase.assertFails(db.collection('stationary').doc('3').update({x: '1'}))
    await firebase.assertFails(db.collection('stationary').doc('3').delete())
  })

  it('Cannot read/write printers', async () => {
    await firebase.assertFails(db.collection('printers').doc('1').get())
    await firebase.assertFails(db.collection('printers').doc('1').update({x: '1'}))
    await firebase.assertFails(db.collection('printers').doc('1').delete())
    await firebase.assertFails(db.collection('printers').doc('2').set({x: '1'}))
  })

  it('Cannot read/write batches or batches/id/campaigns or batches/id/recipients', async () => {
    await firebase.assertFails(db.collection('batches').doc('1').get())
    await firebase.assertFails(db.collection('batches').doc('1').update({x: '1'}))
    await firebase.assertFails(db.collection('batches').doc('1').delete())
    await firebase.assertFails(db.collection('batches').doc('2').set({x: '1'}))

    await firebase.assertFails(db.collection('batches').doc('1').collection('campaigns').doc('1').get())
    await firebase.assertFails(db.collection('batches').doc('1').collection('campaigns').doc('1').update({x: '1'}))
    await firebase.assertFails(db.collection('batches').doc('1').collection('campaigns').doc('1').delete())
    await firebase.assertFails(db.collection('batches').doc('2').collection('campaigns').doc('2').set({x: '1'}))

    await firebase.assertFails(db.collection('batches').doc('1').collection('recipients').doc('1').get())
    await firebase.assertFails(db.collection('batches').doc('1').collection('recipients').doc('1').update({x: '1'}))
    await firebase.assertFails(db.collection('batches').doc('1').collection('recipients').doc('1').delete())
    await firebase.assertFails(db.collection('batches').doc('2').collection('recipients').doc('2').set({x: '1'}))
  })

  it('Cannot read products', async () => {
    await firebase.assertSucceeds(db.collection('products').doc('1').get())
  })

  it('Cannot write products', async () => {
    await firebase.assertFails(db.collection('products').doc('1').update({x: '1'}))
    await firebase.assertFails(db.collection('products').doc('1').delete())
    await firebase.assertFails(db.collection('products').doc('2').set({x: '1'}))
  })

  it('Can read own invoices', async () => {
    await firebase.assertSucceeds(db.collection('invoices').doc('1').get())
  })

  it('Cannot write own invoices', async () => {
    await firebase.assertFails(db.collection('invoices').doc('1').update({x: '1'}))
    await firebase.assertFails(db.collection('invoices').doc('1').delete())
  })

  it('Cannot create invoices', async () => {
    await firebase.assertFails(db.collection('invoices').doc('3').set({x: '1'}))
  })

  it('Cannot read/write other invoices', async () => {
    await firebase.assertFails(db.collection('invoices').doc('2').get())
    await firebase.assertFails(db.collection('invoices').doc('2').update({x: '1'}))
    await firebase.assertFails(db.collection('invoices').doc('2').delete())
  })

  // --- Usage ---
  it('Can read own usage', async () => {
    await firebase.assertSucceeds(db.collection('usage').doc('1').get())
  })

  it('Cannot write own usage', async () => {
    await firebase.assertFails(db.collection('usage').doc('1').update({x: '1'}))
    await firebase.assertFails(db.collection('usage').doc('1').delete())
  })

  it('Cannot create usage', async () => {
    await firebase.assertFails(db.collection('usage').doc('3').set({x: '1'}))
  })

  it('Cannot read/write other usage', async () => {
    await firebase.assertFails(db.collection('usage').doc('2').get())
    await firebase.assertFails(db.collection('usage').doc('2').update({x: '1'}))
    await firebase.assertFails(db.collection('usage').doc('2').delete())
  })

  // --- Shopify ---
  it('Can read but not create/update/delete own shopify', async () => {
    await firebase.assertSucceeds(db.collection('shopify').doc('1').get())
    await firebase.assertFails(db.collection('shopify').doc('1').update({x: '1'}))
    await firebase.assertFails(db.collection('shopify').doc('1').delete())
    await firebase.assertFails(db.collection('shopify').doc('3').set({x: 1}))
  })

  it('Cannot read/update/delete other shopify', async () => {
    await firebase.assertFails(db.collection('shopify').doc('2').get())
    await firebase.assertFails(db.collection('shopify').doc('2').update({x: '1'}))
    await firebase.assertFails(db.collection('shopify').doc('2').delete())
  })

  // --- HubSpot ---
  it('Can read/delete but not create/update own hubspot', async () => {
    await firebase.assertSucceeds(db.collection('hubspot').doc('1').get())
    await firebase.assertFails(db.collection('hubspot').doc('1').update({x: '1'}))
    await firebase.assertSucceeds(db.collection('hubspot').doc('1').delete())
    await firebase.assertFails(db.collection('hubspot').doc('3').set({x: 1}))
  })

  it('Cannot read/update/delete other hubspot', async () => {
    await firebase.assertFails(db.collection('hubspot').doc('2').get())
    await firebase.assertFails(db.collection('hubspot').doc('2').update({x: '1'}))
    await firebase.assertFails(db.collection('hubspot').doc('2').delete())
  })

  // --- Notifications ---
  it('Can read but not create/update/delete own notifications', async () => {
    await firebase.assertSucceeds(db.collection('notifications').doc('1').get())
    await firebase.assertFails(db.collection('notifications').doc('1').update({x: '1'}))
    await firebase.assertFails(db.collection('notifications').doc('1').delete())
    await firebase.assertFails(db.collection('notifications').doc('3').set({x: 1}))
  })

  it('Cannot read/update/delete other notifications', async () => {
    await firebase.assertFails(db.collection('notifications').doc('2').get())
    await firebase.assertFails(db.collection('notifications').doc('2').update({x: '1'}))
    await firebase.assertFails(db.collection('notifications').doc('2').delete())
  })

  // --- Fireway ---
  it('Cannot read/write other fireway', async () => {
    await firebase.assertFails(db.collection('fireway').doc('1').get())
    await firebase.assertFails(db.collection('fireway').doc('1').update({x: '1'}))
    await firebase.assertFails(db.collection('fireway').doc('1').delete())
    await firebase.assertFails(db.collection('fireway').doc('2').set({x: 1}))
  })

  // --- Referral ---
  it('Can create new referral', async () => {
    await firebase.assertSucceeds(db.collection('referral').doc('2').set({x: 1}))
  })

  it('Cannot read/update/delete a referral', async () => {
    await firebase.assertFails(db.collection('referral').doc('1').get())
    await firebase.assertFails(db.collection('referral').doc('1').update({x: '1'}))
    await firebase.assertFails(db.collection('referral').doc('1').delete())
  })

  // --- Misc ---
  it('Cannot read/write unknown collections', async () => {
    await firebase.assertFails(db.collection('unknown_collection').doc('1').set({x: 1}))
  })
})

describe('non authenticated user', () => {
  let db

  beforeAll(async () => {
    db = await db_setup(null, mock_data)
  })

  afterAll(async () => {
    await db_teardown()
  })

  it('Cannot create a new admin user', async () => {
    await firebase.assertFails(db.collection('users').doc(mock_user_new.uid).set({admin: true, name: 'Alex'}))
  })

  it('Can create a new user', async () => {
    await firebase.assertSucceeds(db.collection('users').doc(mock_user_new.uid).set({name: 'Alex'}))
  })

  it('Cannot read/write other users', async () => {
    await firebase.assertFails(db.collection('users').doc(mock_user_non_admin.uid).get())
    await firebase.assertFails(db.collection('users').doc(mock_user_non_admin.uid).update({name: 'Tom'}))
  })
})
