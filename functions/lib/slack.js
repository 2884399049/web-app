const functions = require('firebase-functions')
const configs = functions.config()

const { WebClient } = require('@slack/web-api')

const token = configs.slack.api_key

const web = new WebClient(token)

function post(message, channel) {
  if (configs.environment.name === 'production') {
    console.log('[Slack] posting')
    return web.chat.postMessage({ channel: channel, text: message })
  }
}

module.exports = {
  post
}
