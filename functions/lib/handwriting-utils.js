const functions = require('firebase-functions')
const admin = require('firebase-admin')

const configs = functions.config()
const db = admin.firestore()

const URL = require('url')
const sharp = require('sharp')
const fetch = require('node-fetch')
const SimplexNoise = require('simplex-noise')
const bucket = admin.storage().bucket(configs.firestore.storagebucket)

const layout_utils = require('./layout-utils')

// 12 tokens, 3600 each = 43K per hour
const TOKENS = [
  {
    'key': 'PVBDCG47MGMA06NY',
    'secret': 'X0KA2K1JW0A5NJ9R'
  },
  {
    'key': 'QT3GXH1ZRQNRNWCZ',
    'secret': 'AP7PJNGT5FZM4CYN'
  },
  {
    'key': 'ED918G64X9YRRMYW',
    'secret': '75XS47Z8VQ1Q0DQH'
  },
  {
    'key': 'AAMW2PR8NB5W8H77',
    'secret': 'XTCFGMB0DDAS4Z91'
  },
  {
    'key': 'C8K7R3KWEMPWHRR1',
    'secret': 'ZVHBMK0JGY6GNZSC'
  },
  {
    'key': 'Q088JE4C0DZV520B',
    'secret': '6WVEGZ29BJRWDMX7'
  },
  {
    'key': 'VEF9PPQK7N7PJXNW',
    'secret': 'RK6KS798FG4827HV'
  },
  {
    'key': 'CHM5Y2Q0JFXHEYM5',
    'secret': 'P1CPTZV1NPZC4WVS'
  },
  {
    'key': 'Q7CGTHBKA09ZY8CD',
    'secret': '3VDNN0N54ABXSNTA'
  },
  {
    'key': 'EE6D7EJRWX2EQ48D',
    'secret': '1PMHV0CMTYT6YDW9'
  },
  {
    'key': '924847B595KP3PH1',
    'secret': '8VJV7K631RNES8R7'
  },
  {
    'key': 'NGAV7BNACHSDDRXT',
    'secret': 'D6RT70XDXQA2E2Q8'
  },
  {
    'key': 'KB7BMGHE8J37QY2W',
    'secret': 'VZERYHZM48CRS0CB'
  },
  {
    'key': 'FFBMQHBFFSCHB8ZX',
    'secret': 'K1NXBKV2EQ4546W3'
  },
  {
    'key': 'FE8V2XTTNZHQ5B7V',
    'secret': '36EAJ9A593GMVPTJ'
  },
  {
    'key': 'MH2SK9M7SZF7CTVH',
    'secret': 'C8RVGMSB6EF0S5GN'
  },
  {
    'key': 'FGQH7PWFKRVMBR7K',
    'secret': '08BTG1SY9V8HRCEX'
  },
  {
    'key': '7EFJTQPCZ5FTSZTB',
    'secret': 'YE8FHYDC14FKCPK7'
  },
  {
    'key': 'AVCHV911B1T9939K',
    'secret': '1X9844JM2E7SX8M6'
  },
  {
    'key': 'FC5KTH2ZNA2SCMT3',
    'secret': 'HNG5E7KZ5CS6FXY5'
  }
]

async function request_handwriting (text, handwriting_style, text_size, width, retries, handwriting_colour, handwriting_engine) {
  if (handwriting_style[0] === '$') {
    let _handwriting_style = parseInt(handwriting_style.substr(1))
    let _text_size = parseInt(1.4 * text_size)
    return request_handwriting_engine(text, _handwriting_style, _text_size, width)
  } else if (handwriting_engine) {
    let _text_size = parseInt(1.4 * text_size)
    return request_handwriting_engine(text, handwriting_style, _text_size, width)
  } else {
    return request_hwio(text, handwriting_style, parseInt(layout_utils.mm_to_px(text_size)), parseInt(layout_utils.mm_to_px(width)), retries, handwriting_colour)
  }
}

async function request_hwio (text, handwriting_style, text_size, width, retries, handwriting_colour) {
  var result = {
    success: false,
    error_message: `Failed to render text`,
    error_code: 'error/text-render-failed',
    remaining: undefined,
    text_url: undefined,
    text_image_path: undefined,
    retries: 0
  }

  if (!text || text.length === 0) {
    result.error_message = 'Failed to render text, no text'
    result.error_code = 'error/text-render-failed'
    return result
  }

  if (!handwriting_style) {
    result.error_message = 'Failed to render text, no handwriting style'
    result.error_code = 'error/text-render-failed'
    return result
  }

  // Pick a random key secret pair
  var token = TOKENS[Math.floor(Math.random() * TOKENS.length)]
  console.log('Using token: ', token)

  var auth = Buffer.from(token.key + ':' + token.secret).toString('base64')

  var hwio_url = `https://api.handwriting.io/render/png?handwriting_id=${handwriting_style}&height=auto&handwriting_size=${text_size}px&width=${width}px&text=${encodeURIComponent(text)}`

  console.log(hwio_url)

  let do_retry = false
  try {
    var response = await fetch(hwio_url, {
      method: 'GET',
      headers: {
        'Authorization': `Basic ${auth}`
      },
      timeout: 10000
    })

    if (response.status !== 200 || !response) {
      do_retry = true
      console.log(`HWIO Failed to return 200 - retrying`)
      console.log(response.status)
      console.log(response.statusText)
      console.log(JSON.stringify(await response.json()))
    }
  } catch (err) {
    if (err.type === 'request-timeout') {
      do_retry = true
      console.log(`HWIO timedout - retrying`)
    }
  }

  if (do_retry) {
    if (retries === undefined) {
      result['retries'] = 2
    } else if (retries > 1) {
      result['retries'] = retries - 1
    } else {
      result['retries'] = 0
    }

    console.log(`Retries: ${result['retries']}`)
    return result
  }

  var simplex = new SimplexNoise(Math.random)

  var remaining = parseInt(response.headers.get('x-ratelimit-remaining'))
  console.log('Requests remaining', remaining)

  var img_text = await sharp(await response.buffer())

  var img_text_metadata = await img_text.metadata()
  img_text = await img_text.raw().toBuffer()

  width = img_text_metadata.width
  var height = img_text_metadata.height

  var noise_scale = 96
  for (var x = 0; x < width; x++) {
    for (var y = 0; y < height; y++) {
      var pos = (x + y * width) * img_text_metadata.channels

      if (img_text[pos + 3] <= 0 || img_text[pos + 0] > 0) {
        img_text[pos + 0] = 255
        img_text[pos + 1] = 255
        img_text[pos + 2] = 255
        img_text[pos + 3] = 0
      } else {
        var i = Math.abs(Number(simplex.noise2D(x / noise_scale, y / noise_scale)) + 0.5)

        var r = i * 35 // 2, 93, 169
        var g = i * 38 // 0, 44, 120
        var b = i * 49 + 60

        if (handwriting_colour && handwriting_colour === 'black') {
          r = i * 40
          g = i * 40
          b = i * 40
        }

        if (r > 255) r = 255
        if (g > 255) g = 255
        if (b > 255) b = 255

        img_text[pos + 0] = r
        img_text[pos + 1] = g
        img_text[pos + 2] = b
        img_text[pos + 3] = 255
      }
    }
  }

  // Generate random ID
  var text_id = db.collection('random').doc().id + parseInt(Math.random() * 10000000)
  var text_image_path = `/tmp/${text_id}.jpeg`

  await sharp(img_text, {raw: {width: width, height: height, channels: img_text_metadata.channels}}).removeAlpha().jpeg().toFile(text_image_path)

  await bucket.upload(text_image_path, {destination: `text/${text_id}.jpeg`})
  var url = `https://storage.googleapis.com/${configs.firestore.storagebucket}/text/${text_id}.jpeg`

  console.log(url)

  return {
    success: true,
    remaining: remaining,
    text_url: url,
    text_image_path: text_image_path
  }
}

async function request_handwriting_engine (text, handwriting_style, text_size, width) {
  var params = {
    ppi: 600,
    font_size_mm: parseInt(text_size),
    width_mm: parseInt(width),
    text: text,
    line_height: 1.2,
    handwriting_style: handwriting_style // Or font_url: URL
  }

  const qs = new URL.URLSearchParams(params)

  try {
    console.log(`${configs.handwriting_engine.api_url}api/write?` + qs)
    var response = await fetch(`${configs.handwriting_engine.api_url}api/write?` + qs, {
      method: 'get',
      timeout: 10000
    })

    if (response.status !== 200 || !response) {
      console.log(`Handwriting Engine Failed to return 200`)
      console.log(response.status)
      console.log(response.statusText)

      return {
        success: false,
        error_message: `Failed to render text`,
        error_code: 'error/text-render-failed',
        text_url: undefined,
        text_image_path: undefined,
        retries: 0,
        remaining: true
      }
    }
  } catch (err) {
    if (err.type === 'request-timeout') {
      console.log(`Handwriting Engine timeout`)
    }
  }

  let out
  try {
    out = await response.buffer()
  } catch (err) {
    return {
      success: false,
      error_message: `Failed to render text`,
      error_code: 'error/text-render-failed',
      text_url: undefined,
      text_image_path: undefined,
      retries: 0,
      remaining: true
    }
  }

  let text_id = db.collection('random').doc().id + parseInt(Math.random() * 10000000)
  let text_image_path = `/tmp/${text_id}.jpeg`
  let img_text = await sharp(out).removeAlpha().jpeg().toFile(text_image_path)

  await bucket.upload(text_image_path, {destination: `text/${text_id}.jpeg`})
  var url = `https://storage.googleapis.com/${configs.firestore.storagebucket}/text/${text_id}.jpeg`

  return {
    success: true,
    text_url: url,
    text_image_path: text_image_path,
    retries: 0,
    remaining: true
  }
}

module.exports = {
  request_hwio,
  request_handwriting_engine,
  request_handwriting
}
