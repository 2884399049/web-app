const functions = require('firebase-functions')
const admin = require('firebase-admin')

const PDFDocument = require('pdfkit')
const utils = require('./utils')
const constants = require('../shared/constants')
const download = require('image-downloader')
const gs = require('gs')
const sharp = require('sharp')
const fs = require('fs')
const puppeteer = require('puppeteer');
const jsdom = require("jsdom")
const { JSDOM } = jsdom
const HummusRecipe = require('hummus-recipe')

const configs = functions.config()
const bucket = admin.storage().bucket(configs.firestore.storagebucket)

let BROWSER

function pdf_mm_to_px (mm) {
  // Default PPI is 72 per inch for PDF
  return mm * constants.MM_IN_PX * 72
}

function mm_to_px (mm, PPI) {
  if (!PPI) {
    PPI = 600
  }
  return parseInt(parseInt(mm) * constants.MM_IN_PX * PPI)
}

/*
  - text_image = path to image
  - width = width, height,
  - location = `/recipients/${blah}/letter.pdf` or enveloped.
*/
async function layout_letter (text_image, valign, width, height, text_x, text_y, text_width, text_height, location, background, back_image) {
  var doc = new PDFDocument({size: [pdf_mm_to_px(width), pdf_mm_to_px(height)]})

  var stream = doc.pipe(bucket.file(location).createWriteStream({public: true}))

  if (background) {
    doc.image(background, 0, 0, {width: pdf_mm_to_px(width), height: pdf_mm_to_px(height)})
  }

  if (text_width && text_height) {
    doc.image(text_image, pdf_mm_to_px(text_x), pdf_mm_to_px(text_y), {fit: [pdf_mm_to_px(text_width), pdf_mm_to_px(text_height)], align: 'center', valign: valign})
  } else {
    doc.image(text_image, pdf_mm_to_px(text_x), pdf_mm_to_px(text_y))
  }

  if (back_image) {
    doc.addPage()
    doc.image(back_image, 0, 0, {fit: [pdf_mm_to_px(width), pdf_mm_to_px(height)], align: 'center', valign: 'center'})
  }

  // await new Promise(resolve => setTimeout(() => resolve(), 100));

  doc.end()
  await utils.end_stream(stream)

  var url = `https://storage.googleapis.com/${configs.firestore.storagebucket}/${location}?t=${Date.now()}`
  return url
}
//
// async function pdf_to_png (campaign_id, recipient_id, url, is_us) {
//   let pdf_path = `/tmp/${recipient_id}.pdf`
//   let png_path = `/tmp/${recipient_id}.png`
//   let mockup_path = `/tmp/${recipient_id}-mockup.jpg`
//
//   await download.image({url: url, dest: pdf_path})
//
//   await new Promise((resolve, reject) => {
//     gs()
//       .batch()
//       .nopause()
//       .option('-r' + 400)
//       .option('-dDownScaleFactor=1')
//       .executablePath('lambda-ghostscript/bin/./gs')
//       .device('png16m')
//       .output(png_path)
//       .input(pdf_path)
//       .exec((err, stdout, stderr) => {
//         if (!err) {
//           console.log('gs executed w/o error')
//           console.log('stdout', stdout)
//           console.log('stderr', stderr)
//           resolve()
//         } else {
//           console.log('gs error:', err)
//           reject(err)
//         }
//       })
//   })
//
//   let location = `campaigns/${campaign_id}/recipients/${recipient_id}/${campaign_id}-${recipient_id}-letter.png`
//   await bucket.upload(png_path, {destination: location})
//
//   let mockup_template = is_us ? './assets/mockup_us.jpg' : './assets/mockup_gb.jpg'
//
//   let img_background = await sharp(mockup_template).resize(7000)
//   await img_background.composite([{ input: png_path, blend: 'darken', top: 1000, left: 1685 }]).jpeg().toFile(mockup_path)
//
//   let mockup_location = `campaigns/${campaign_id}/recipients/${recipient_id}/${campaign_id}-${recipient_id}-letter-mockup.jpg`
//   await bucket.upload(mockup_path, {destination: mockup_location})
//
//   return {
//     png: `https://storage.googleapis.com/${configs.firestore.storagebucket}/${location}?t=${Date.now()}`,
//     mockup: `https://storage.googleapis.com/${configs.firestore.storagebucket}/${mockup_location}?t=${Date.now()}`
//   }
// }

async function upload_image(location, file_path, retry) {
  try {
    await bucket.upload(file_path, {
      destination:  location
    })
  } catch (err) {
    console.warn(`Error: Failed to upload ${file_path} to ${location}`, err)

    if (!retry) {
      return await upload_image(location, file_path, true)
    } else {
      throw new Error(`Error: Failed to upload`)
    }
  }


  return `https://storage.googleapis.com/${configs.firestore.storagebucket}/${location}?t=${parseInt(Math.random() * 10000000000)}`
}

function modify_stationary_html(stationary, _html, no_bleed, alter_paper_to, text_image_url, no_padding) {
  let bleed_mark_margin = '2mm'

  let html = `
  <html>
    <body style="margin: 0px;">
      <div id="paper-container">
        <div id="bleed-marks" style="display: none;">
          <div style="box-sizing: border-box; height: ${constants.BLEED}mm; width: ${constants.BLEED}mm; position: absolute; top: 0px; left: 0px; pointer-events: none; border-style: solid; border-color: black; border-right-width: 1px; border-bottom-width: 1px; border-left-width: 0px; border-top-width: 0px;"></div>
          <div style="height: ${bleed_mark_margin}; width: ${bleed_mark_margin}; position: absolute; top: calc(${constants.BLEED}mm - ${bleed_mark_margin}); left: calc(${constants.BLEED}mm - ${bleed_mark_margin}); pointer-events: none; background: white;"></div>

          <div style="box-sizing: border-box; height: ${constants.BLEED}mm; width: ${constants.BLEED}mm; position: absolute; bottom: 0px; left: 0px; pointer-events: none; border-style: solid; border-color: black; border-right-width: 1px; border-top-width: 1px; border-left-width: 0px; border-bottom-width: 0px;"></div>
          <div style="height: calc(${bleed_mark_margin} + 1px); width: calc(${bleed_mark_margin} + 1px); position: absolute; bottom: calc(${constants.BLEED}mm - ${bleed_mark_margin}); left: calc(${constants.BLEED}mm - ${bleed_mark_margin}); pointer-events: none; background: white;"></div>

          <div style="box-sizing: border-box; height: ${constants.BLEED}mm; width: ${constants.BLEED}mm; position: absolute; top: 0px; right: 0px; pointer-events: none; border-style: solid; border-color: black; border-left-width: 1px; border-bottom-width: 1px; border-right-width: 0px; border-top-width: 0px;"></div>
          <div style="height: ${bleed_mark_margin}; width: ${bleed_mark_margin}; position: absolute; top: calc(${constants.BLEED}mm - ${bleed_mark_margin}); right: calc(${constants.BLEED}mm - ${bleed_mark_margin}); pointer-events: none; background: white;"></div>

          <div style="box-sizing: border-box; height: ${constants.BLEED}mm; width: ${constants.BLEED}mm; position: absolute; bottom: 0px; right: 0px; pointer-events: none; border-style: solid; border-color: black; border-left-width: 1px; border-top-width: 1px; border-right-width: 0px; border-bottom-width: 0px;"></div>
          <div style="height: calc(${bleed_mark_margin} + 1px); width: calc(${bleed_mark_margin} + 1px); position: absolute; bottom: calc(${constants.BLEED}mm - ${bleed_mark_margin}); right: calc(${constants.BLEED}mm - ${bleed_mark_margin}); pointer-events: none; background: white;"></div>
        </div>
        ${_html}
      </div>
    </body>
  </html>`

  const dom = new JSDOM(html)
  let paper_preview = dom.window.document.querySelector('.paper-preview')

  dom.window.document.querySelector('#bleed-marks').style['display'] = 'none'

  let paper_width = stationary.paper.width
  let paper_height = stationary.paper.height

  if (alter_paper_to) {
    paper_width = alter_paper_to.width
    paper_height = alter_paper_to.height

    dom.window.document.querySelector('#paper-container').style['position'] = `absolute`

    if (paper_preview) {
      paper_preview.style['transform'] = `scale(${stationary.paper.scale})`
      paper_preview.style['position'] = `relative`
      paper_preview.style['margin'] = `auto`
      paper_preview.style['transform-origin'] = `0 0`

      if (alter_paper_to.top) {
        dom.window.document.querySelector('.paper-preview').style['top'] = `${alter_paper_to.top}mm`
      } else {
        dom.window.document.querySelector('.paper-preview').style['transform-origin'] = `center 0`
      }
    }
  }

  let width = parseFloat(paper_width) + constants.BLEED * 2
  let height = parseFloat(paper_height) + constants.BLEED * 2
  let margin = constants.BLEED

  if (no_padding) {
    // width = paper_width
    // height = paper_height
    margin = 0
  }

  if (!no_bleed) {
    dom.window.document.querySelector('#paper-container').style['width'] = `${width}mm`
    dom.window.document.querySelector('#paper-container').style['height'] = `${height}mm`

    if (no_padding) {
      paper_preview.style['width'] = `${width}mm`
      paper_preview.style['height'] = `${height}mm`
    }

    dom.window.document.querySelector('#bleed-marks').style['width'] = `${width}mm`
    dom.window.document.querySelector('#bleed-marks').style['height'] = `${height}mm`
    dom.window.document.querySelector('#bleed-marks').style['position'] = 'absolute'
    dom.window.document.querySelector('#bleed-marks').style['display'] = 'block'

    if (alter_paper_to) {
      if (alter_paper_to.top) {
        dom.window.document.querySelector('.paper-preview').style['top'] = `${alter_paper_to.top + constants.BLEED}mm`
      } else {
        dom.window.document.querySelector('.paper-preview').style['top'] = `${constants.BLEED}mm`
      }
    } else {
      dom.window.document.querySelector('.paper-preview').style['margin'] = `${margin}mm`
    }
  } else {
    dom.window.document.querySelector('#paper-container').style['width'] = `${paper_width}mm`
    dom.window.document.querySelector('#paper-container').style['height'] = `${paper_height}mm`
  }

  let handwriting_image_ele = dom.window.document.querySelector("#handwriting-image")

  if (handwriting_image_ele) {
    dom.window.document.querySelector("#handwriting-image").src = text_image_url
    dom.window.document.querySelector("#handwriting-image").style['object-fit'] = 'contain'
    dom.window.document.querySelector("#handwriting-image").style['object-position'] = 'top'
    dom.window.document.querySelector("#handwriting-image").style['height'] = '100%'
    dom.window.document.querySelector("#handwriting-image").style['background-image'] = ""
  }

  if (!text_image_url && handwriting_image_ele) {
    dom.window.document.querySelector("#handwriting-image").remove()
  }

  let new_html = dom.serialize()
  new_html = new_html.replace('background-image: linear-gradient(45deg, rgb(232, 232, 232) 25%, rgb(227, 227, 227) 25%, rgb(227, 227, 227) 50%, rgb(232, 232, 232) 50%, rgb(232, 232, 232) 75%, rgb(227, 227, 227) 75%, rgb(227, 227, 227) 100%);', '')

  return new_html
}

function random_path(extension) {
  return `/tmp/${parseInt(Math.random() * 10000000000)}.${extension}`
}

/**
 * Merge two pdfs
 *
 * @param {*} front_pdf_path
 * @param {*} back_pdf_path
 * @returns temporary pdf path for merged pdf
 */
async function merge_pdf(front_pdf_path, back_pdf_path) {
  let merge_pdf_name = random_path('pdf')

  const pdfDoc = new HummusRecipe(front_pdf_path, merge_pdf_name)
  pdfDoc
    .appendPage(back_pdf_path)
    .endPDF()

  return merge_pdf_name
}

async function load_html_page(html) {
  if (!BROWSER) { // cache puppeteer browser
    BROWSER = await puppeteer.launch({headless: true, args: ['--no-sandbox', '--disable-setuid-sandbox']})
  }

  let page = await BROWSER.newPage()
  await page.setContent(html, {
    waitUntil: 'networkidle0',
  })

  return page
}

async function render_html_to_pdf(html, width, height, add_bleed) {
  let page = await load_html_page(html)
  let path = random_path('pdf')

  let bleed = constants.BLEED
  if (add_bleed === false) {
    bleed = 0
  }

  let page_width = parseFloat(width) + parseFloat(bleed)*2
  let page_height = parseFloat(height) + parseFloat(bleed)*2

  await page.pdf({
    path: path,
    width: `${page_width}mm`,
    height: `${page_height}mm`,
    printBackground: true,
    pageRanges: '1',
    margin: {top: 0, right: 0, bottom: 0, left: 0},
    displayHeaderFooter: false
  })

  return path
}

async function render_html_to_png(html, width, height, add_bleed) {
  let page = await load_html_page(html)
  let path = random_path('png')

  let bleed = constants.BLEED
  if (add_bleed === false) {
    bleed = 0
  }

  let page_width = parseFloat(width) + parseFloat(bleed)*2
  let page_height = parseFloat(height) + parseFloat(bleed)*2

  let element = await page.$('#paper-container')
  await element.screenshot({
    path: path,
    printBackground: true
  })

  return path
}

module.exports = {
  layout_letter,
  mm_to_px,
  pdf_mm_to_px,
  load_html_page,
  render_html_to_png,
  render_html_to_pdf,
  merge_pdf,
  upload_image,
  modify_stationary_html
}
