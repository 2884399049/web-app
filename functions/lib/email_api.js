const fs = require('fs').promises
const readline = require('readline')
const {google} = require('googleapis')
const request = require('request-promise-native')

const functions = require('firebase-functions')
const configs = functions.config()

// If modifying these scopes, delete token.json.
const SCOPES = ['https://www.googleapis.com/auth/gmail.readonly', 'https://www.googleapis.com/auth/gmail.modify']
// The file token.json stores the user's access and refresh tokens, and is
// created automatically when the authorization flow completes for the first
// time.
const TOKEN_PATH = 'token.json'

const credentials = {'installed': {'client_id': '234970627004-8k7oma4gehpn0au0ii6gpej2357h7iu2.apps.googleusercontent.com', 'project_id': 'scribeless-email-1574867679236', 'auth_uri': 'https://accounts.google.com/o/oauth2/auth', 'token_uri': 'https://oauth2.googleapis.com/token', 'auth_provider_x509_cert_url': 'https://www.googleapis.com/oauth2/v1/certs', 'client_secret': 'oyPLI9EgYEzDjbv5FDtVN6mF', 'redirect_uris': ['urn:ietf:wg:oauth:2.0:oob', 'http://localhost']}}
const TOKEN = {'access_token': 'ya29.Il-yB-N6R2jubyG4D8lIF9_83RRAK_rNMsrbG7vhmV7Uajwu3Vta-jLDF9OFvOCJiGhiBkhDl6zyEtfQoH2z_kIq2ohkRqsQhaGk3zuIYBofr2l9ECTHl9p2-04UBvHogA', 'refresh_token': '1//03n605HJ5J4--CgYIARAAGAMSNwF-L9Ira2_gAcl3IZbTZLAFUgRWIJyrx5-2du_xqcI_Q8kpfrRb2ySg8pxjsB3CBdCAxWiNJYo', 'scope': 'https://www.googleapis.com/auth/gmail.readonly https://www.googleapis.com/auth/gmail.modify', 'token_type': 'Bearer', 'expiry_date': 1574942259872}

async function fetch_emals () {
  console.log('fetch_emals')

  if (configs.environment.name !== 'production') {
    console.log(`Not on production env, not fetching emails`)
    return false
  }

  console.log('fetch_emals2')

  // const {client_secret, client_id, redirect_uris} = credentials.installed;
  // const oAuth2Client = new google.auth.OAuth2(client_id, client_secret, redirect_uris[0])
  // return listThreads(oAuth2Client.setCredentials(TOKEN))

  const auth = new google.auth.GoogleAuth({
    // Scopes can be specified either as an array or as a single, space-delimited string.
    scopes: SCOPES
  })
  const authClient = await auth.getClient()
  return listThreads(authClient)

  // return authorize(credentials).then(listThreads)
}

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize (credentials, callback) {
  const {client_secret, client_id, redirect_uris} = credentials.installed
  const oAuth2Client = new google.auth.OAuth2(
    client_id, client_secret, redirect_uris[0])

  // Check if we have previously stored a token.
  return fs.readFile(TOKEN_PATH).then((err, token) => {
    if (err) return getNewToken(oAuth2Client, callback)
    oAuth2Client.setCredentials(JSON.parse(token))
    return oAuth2Client
  })
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback for the authorized client.
 */
function getNewToken (oAuth2Client, callback) {
  const authUrl = oAuth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: SCOPES
  })
  console.log('Authorize this app by visiting this url:', authUrl)
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  })
  rl.question('Enter the code from that page here: ', (code) => {
    rl.close()
    oAuth2Client.getToken(code, (err, token) => {
      if (err) return console.error('Error retrieving access token', err)
      oAuth2Client.setCredentials(token)
      // Store the token to disk for later program executions
      fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
        if (err) return console.error(err)
        console.log('Token stored to', TOKEN_PATH)
      })
      callback(oAuth2Client)
    })
  })
}

function handle_error (err) {
  console.warn(`Error:`, err)
}

function handle_order_email (gmail, message) {
  let subject = ''
  let date = ''
  let from = ''
  let text = ''

  for (let part of message.payload.parts) {
    if (part.mimeType !== 'text/plain') {
      continue
    }
    let buff = new Buffer.from(part.body.data, 'base64')
    text = buff.toString('ascii')
    // console.log(part)
  }

  for (let header of message.payload.headers) {
    if (header.name === 'Subject') {
      subject = header.value
    }

    if (header.name === 'Date') {
      date = header.value
    }

    if (header.name === 'From') {
      from = header.value
    }
  }

  console.log(`Recived email from: ${from} at ${date}`)

  if (!subject.match(/[\w\d]{20}/)) {
    console.warn(`No campaign ID in subject! Subject: "${subject}"`)
    return
  }

  let fields = ['first_name', 'last_name', 'title', 'address_line_1', 'address_line_2', 'address_line_3', 'city', 'zip', 'state', 'country', 'department']
  let data = {}
  for (let field of fields) {
    let regex = new RegExp(`${field.toUpperCase()}:(.+)`)
    let groups = text.match(regex)
    if (groups && groups.length === 2) {
      data[field] = groups[1].trim()
    }
  }

  console.log(`Attempting to add to campaign: ${subject}, data:\n`, data, ``)

  let recipient = {}
  if (data['address_line_1']) recipient['address line 1'] = data['address_line_1']
  if (data['address_line_2']) recipient['address line 2'] = data['address_line_2']
  if (data['address_line_3']) recipient['address line 3'] = data['address_line_3']
  if (data['city']) recipient['city'] = data['city']
  if (data['country']) recipient['country'] = data['country']
  if (data['department']) recipient['department'] = data['department']
  if (data['first_name']) recipient['first name'] = data['first_name']
  if (data['last_name']) recipient['last name'] = data['last_name']
  if (data['state']) recipient['state/region'] = data['state']
  if (data['title']) recipient['title'] = data['title']
  if (data['zip']) recipient['zip/postal code'] = data['zip']

  let recipients = [recipient]

  const API_key = 'pZEdpMgNnOO4RNi64v88ixYqf0R2$otzMNjdidNRtPZ0yC9Ah' // TODO note this is ridealtoa api key
  return request({
    method: 'PUT',
    json: true,
    uri: `https://us-central1-hc-application-interface-prod.cloudfunctions.net/api/v1/campaign/${subject}?api_key=${API_key}`,
    body: {
      recipients: recipients
    }
  }).then((response) => {
    if (!response.success) {
      console.warn(response)
      markMessageAsFailed(gmail, message.id)
    } else {
      console.log(`Successfully added`)
      markMessageAsComplete(gmail, message.id)
    }
  }).catch((err) => {
    console.warn(err.error)
    markMessageAsFailed(gmail, message.id)
  })
}

function getThreadResponse (gmail, err, res) {
  if (err) return handle_error(err)
  const thread = res.data
  let proms = []
  for (let message of thread.messages) {
    // console.log(message.payload)
    for (let header of message.payload.headers) {
      if (header.name === 'To') {
        if (header.value.match(/orders@scribeless.co/)) {
          proms.push(handle_order_email(gmail, message))
        }
      }
    }
  }

  return Promise.all(proms)
}

function listLabelsResponse (gmail, err, res) {
  if (err) return handle_error(err)
  const threads = res.data.threads

  let proms = []
  if (threads.length) {
    threads.forEach((thread) => {
      proms.push(getThread(gmail, thread.id))
    })
  } else {
    console.log('No labels found.')
  }

  return Promise.all(proms)
}

function listLabels (auth) {
  const gmail = google.gmail({version: 'v1', auth})
  return gmail.users.labels.list({
    userId: 'me'
  }).then((err, res) => {
    if (err) return console.log('The API returned an error: ' + err)
    const labels = res.data.labels
    if (labels.length) {
      console.log('Labels:')
      labels.forEach((label) => {
        console.log(`- ${label.name} - ${label.id}`)
      })
    } else {
      console.log('No labels found.')
    }
  })
}

function listThreads (auth) {
  const gmail = google.gmail({version: 'v1', auth})
  console.log(`listThreads`)
  return gmail.users.threads.list({userId: 'me', labelIds: ['INBOX']}).then(listLabelsResponse.bind(null, gmail))
}

function markMessageAsComplete (gmail, id) {
  return gmail.users.messages.modify({id: id, userId: 'me', resource: {addLabelIds: ['Label_7285786925468081333'], removeLabelIds: ['INBOX']}})
}

function markMessageAsFailed (gmail, id) {
  return gmail.users.messages.modify({id: id, userId: 'me', resource: {addLabelIds: ['Label_8274037201473896689'], removeLabelIds: ['INBOX']}})
}

function getThread (gmail, threadId) {
  return gmail.users.threads.get({
    'userId': 'me',
    'id': threadId
  }).then(getThreadResponse.bind(null, gmail))
}

async function parse_email (req, res) {
  console.log(req.body)

  if (configs.environment.name !== 'production') {
    console.log(`Not on production env, not fetching emails`)
    return res.status(400).send('Failed')
  }

  let subject = req.body.subject
  let from = req.body.from_email
  let text = req.body.body_plain

  console.log(`Recived email from: ${from}`)

  if (!subject.match(/[\w\d]{20}/)) {
    console.warn(`No campaign ID in subject! Subject: "${subject}"`)
    return res.status(400).send('Failed')
  }

  let fields = ['first_name', 'last_name', 'title', 'address_line_1', 'address_line_2', 'address_line_3', 'city', 'zip', 'state', 'country', 'department']
  let data = {}
  for (let field of fields) {
    let regex = new RegExp(`${field.toUpperCase()}:(.+)`)
    let groups = text.match(regex)
    if (groups && groups.length === 2) {
      data[field] = groups[1].trim()
    }
  }

  console.log(`Attempting to add to campaign: ${subject}, data:\n`, data, ``)

  let recipient = {}
  if (data['address_line_1']) recipient['address line 1'] = data['address_line_1']
  if (data['address_line_2']) recipient['address line 2'] = data['address_line_2']
  if (data['address_line_3']) recipient['address line 3'] = data['address_line_3']
  if (data['city']) recipient['city'] = data['city']
  if (data['country']) recipient['country'] = data['country']
  if (data['department']) recipient['department'] = data['department']
  if (data['first_name']) recipient['first name'] = data['first_name']
  if (data['last_name']) recipient['last name'] = data['last_name']
  if (data['state']) recipient['state/region'] = data['state']
  if (data['title']) recipient['title'] = data['title']
  if (data['zip']) recipient['zip/postal code'] = data['zip']

  let recipients = [recipient]

  const API_key = 'pZEdpMgNnOO4RNi64v88ixYqf0R2$otzMNjdidNRtPZ0yC9Ah' // TODO note this is ridealtoa api key

  let response
  try {
    response = await request({
      method: 'PUT',
      json: true,
      uri: `https://us-central1-hc-application-interface-prod.cloudfunctions.net/api/v1/campaign/${subject}?api_key=${API_key}`,
      body: {
        recipients: recipients
      }
    })
  } catch (err) {
    console.warn(`Failed to add email ${subject}`)
    console.warn(err.error)

    return res.status(400).send('Failed')
  }

  if (!response.success) {
    console.warn(`Failed to add email ${subject}`)
    console.warn(response)
    // markMessageAsFailed(gmail, message.id)
    return res.status(400).send('Failed')
  } else {
    console.log(`Successfully added`)
    // markMessageAsComplete(gmail, message.id)
  }

  return res.send('Success')
}

module.exports = {
  fetch_emals,
  parse_email
}
