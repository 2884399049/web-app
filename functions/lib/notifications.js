const functions = require('firebase-functions')
const admin = require('firebase-admin')
const utils = require('./utils')
const slack = require('./slack')
const constants = require('../shared/constants')

const configs = functions.config()
const db = admin.firestore()

async function notify () {
  let uids = {}

  let notifications_query = await db.collection('notifications').where('status', '==', constants.NOTIFICATION_STATUS.new).get()
  notifications_query.forEach((notifications_doc) => {
    console.log(`Found notifications_doc ${notifications_doc.id}`)
    notification = notifications_doc.data()
    notification.id = notifications_doc.id

    if (!uids[notification.uid]) {
      uids[notification.uid] = []
    }

    uids[notification.uid].push(notification)
  })

  let promises = []
  for (let uid in uids) {
    let user = await utils.get_user_profile(uid)

    let message = ''
    for (let notification of uids[uid]) {
      message += `<li><strong>${notification.title}</strong> - ${notification.message}</li>`
      promises.push(db.collection('notifications').doc(notification.id).update({'status': constants.NOTIFICATION_STATUS.notified}))
      console.debug(`Notification ${notification.id}`)
    }

    console.log(`Sending notification to ${user.email} (${uid})`)

    promises.push(utils.send_email(user.email, `Scribeless: Notifications from your account`, `Dear ${user.first_name},<br>Your <a href="https://app.scribeless.co">Scribeless account</a> has notifications:<br><ul>${message}</ul>`))
  }

  return Promise.all(promises)
}

async function add(uid, level, title, message) {
  let promises = []
  promises.push(db.collection('notifications').doc().set({uid: uid, status: constants.NOTIFICATION_STATUS.new, level: level, title: title, message: message, created: new Date()}))
  promises.push(slack.post(`New notificaiton for ${uid} - ${level} - ${title}`, configs.slack.channels.user_notifications))

  return Promise.all(promises)
}

module.exports = {
  notify,
  add
}
