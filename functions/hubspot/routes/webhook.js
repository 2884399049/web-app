const router = require("express").Router();
const admin = require("firebase-admin");
const { contactToAddress, getAccessToken } = require("../functions");
const {
	getContactDataByVid,
	addTimelineEvent
} = require("../functions/hubspotAPI");
const constants = require("../../shared/constants");
const notifications = require("../../lib/notifications");
const bodyParser = require("body-parser");
router.use(bodyParser.json());

const db = admin.firestore();
const Hubspot = db.collection("hubspot");

let apiFunctions;

router.post("/contact", async (req, res) => {
	res.sendStatus(200);

	const { campaign_id } = req.query;
	if (!campaign_id) {
		console.log(
			`hubspot/webhook/contact: query parameter "campaign_id" is empty`
		);
		return;
	}

	const { vid, "portal-id": hubId } = req.body;

	if (!hubId) {
		console.error(
			`hubspot/webhook/contact: request body does not contain portal-id (hubId)`
		);
		return;
	}

	const hubDocRef = await Hubspot.doc(String(hubId));
	const hubDoc = await hubDocRef.get();

	if (!hubDoc.exists || !hubDoc.data().uid) {
		console.log(
			`hubspot/webhook/contact: there is no connected account to "${hubId}" hub id`
		);
		return;
	}

	let uid = hubDoc.data().uid;
	try {
		const accessToken = await getAccessToken(hubDoc);

		const contact = await getContactDataByVid(vid, accessToken);
		const recipient = contactToAddress(contact);

		await addTimelineEvent(vid, accessToken, campaign_id);

		let result = await apiFunctions.add_recipients(
			campaign_id,
			[recipient],
			uid,
			undefined,
			constants.RECIPIENT_SOURCE.hubspot
		);

		console.log(result);

		if (!result.success) {
			console.debug(
				`Failed to add_recipients "${result.error_message}", notifying ${uid}`
			);
			await notifications.add(
				uid,
				constants.NOTIFICATION_LEVEL.error,
				"Invalid Hubspot contact",
				`A hubspot contact has insufficent details to send a letter: "${result.error_message}". Please update the contact at https://hubspot.com/contacts/${hubId}/contact/${vid} and retrigger the letter`
			);
		}
	} catch (e) {
		console.error("hubspot/webhook/contact: error: ", e);
		debugger;
	}
});

router.post("/add_recipient", async (req, res) => {
	res.sendStatus(200);

	const campaign_id = req.body.fields.campaign_id;
	if (!campaign_id) {
		console.log(
			`hubspot/webhook/add_recipient: body parameter "campaign_id" is empty`
		);
		return;
	}

	const hubId = req.body.origin.portalId;
	if (!hubId) {
		console.error(
			`hubspot/webhook/add_recipient: request body does not contain portal-id (hubId)`
		);
		return;
	}

	const hubDocRef = await Hubspot.doc(String(hubId));
	const hubDoc = await hubDocRef.get();

	if (!hubDoc.exists || !hubDoc.data().uid) {
		console.log(
			`hubspot/webhook/add_recipient: there is no connected account to "${hubId}" hub id`
		);
		return;
	}

	let uid = hubDoc.data().uid;
	try {
		const accessToken = await getAccessToken(hubDoc);

		const vid = req.body.object.objectId;
		const contact = await getContactDataByVid(vid, accessToken);
		const recipient = contactToAddress(contact);

		if (req.body.fields.custom_1) {
			recipient["custom 1"] = req.body.fields.custom_1;
		}

		if (req.body.fields.custom_2) {
			recipient["custom 2"] = req.body.fields.custom_2;
		}

		await addTimelineEvent(vid, accessToken, campaign_id);

		let result = await apiFunctions.add_recipients(
			campaign_id,
			[recipient],
			uid,
			undefined,
			constants.RECIPIENT_SOURCE.hubspot
		);

		console.log(result);

		if (!result.success) {
			console.debug(
				`Failed to add_recipients "${result.error_message}", notifying ${uid}`
			);
			await notifications.add(
				uid,
				constants.NOTIFICATION_LEVEL.error,
				"Invalid Hubspot contact",
				`A hubspot contact has insufficent details to send a letter: "${result.error_message}". Please update the contact at https://hubspot.com/contacts/${hubId}/contact/${vid} and retrigger the letter`
			);
		}
	} catch (e) {
		console.error("hubspot/webhook/add_recipient: error: ", e);
		debugger;
	}
});

router.post("/campaign_options", async (req, res) => {
	console.log(req.body);

	const hubId = req.body.portalId;
	if (!hubId) {
		console.error(
			`hubspot/webhook/campaign_options: request body does not contain portal-id (hubId)`
		);
		return;
	}

	const hubDocRef = await Hubspot.doc(String(hubId));
	const hubDoc = await hubDocRef.get();

	if (!hubDoc.exists || !hubDoc.data().uid) {
		console.log(
			`hubspot/webhook/campaign_options: there is no connected account to "${hubId}" hub id`
		);
		return;
	}

	let uid = hubDoc.data().uid;

	let result = {
		options: {
			campaign_id: {}
		}
	};

	let campaigns = await apiFunctions.getUserCampaigns(uid);
	for (let campaign of campaigns) {
		result.options.campaign_id[campaign.title] = campaign.id;
	}

	return res.status(200).json(result);
});

module.exports = function(functions) {
	apiFunctions = functions;
	return router;
};
