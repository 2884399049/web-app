const functions = require('firebase-functions')
const hubspotAPI = require('./hubspotAPI')

const configs = functions.config()

const {
  client_id: CLIENT_ID, client_secret: CLIENT_SECRET, scopes, api_address: SERVICE_ADDRESS,
  scopes: SCOPES
} = configs.hubspot

const {
  HUBSPOT: {
    BASE_ROUTE, CALLBACK_ROUTE
  }
} = require('../../shared/constants')

const _ = {}

function getRedirectUrl () {
  return `${SERVICE_ADDRESS}${BASE_ROUTE}${CALLBACK_ROUTE}`
}

function required (paramName) {
  throw new Error(`Parameter "${paramName}" is required`)
}

const rd = required

_.getRedirectUrl = getRedirectUrl

_.getHubspotAuthUrl = () => {
  const authUrl =
    'https://app.hubspot.com/oauth/authorize' +
    `?client_id=${encodeURIComponent(CLIENT_ID)}` + // app's client ID
    `&scope=${encodeURIComponent(SCOPES)}` + // scopes being requested by the app
    `&redirect_uri=${encodeURIComponent(getRedirectUrl())}` // where to send the user after the consent page

  return authUrl
}

_.contactToAddress = (contact = rd('contact')) => {
  const {
    properties: {
      firstname: { value: firstname } = {},
      lastname: { value: lastname } = {},
      address: { value: address1 } = {},
      country: { value: country } = {},
      state: { value: state } = {},
      city: { value: city } = {},
      zip: { value: zip } = {},
      company: { value: company } = {}
    } = {}
  } = contact

  const recipient = {
    'postal_code': zip || null,
    city: city || null,
    'first name': firstname || null,
    'last name': lastname || null,
    country: country || null,
    company: company || '',
    'region': state || '',
    // department: department || '',
    'address line 1': address1 || null,
    'address line 2': '',
    'address line 3': ''
  }

  return recipient
}

const exchangeForTokens = async (exchangeProof) => {
  try {
    const tokens = await hubspotAPI.exchangeForTokens(exchangeProof)
    const { refresh_token, access_token, expires_in } = tokens

    console.log('  > Received an access token and refresh token')

    return { refresh_token, access_token, expires_in: Date.now() + expires_in * 1000 }
  } catch (e) {
    console.error('Error exchanging error=', e)
    console.error(`  > Error exchanging ${exchangeProof.grant_type} for access token`)

    let message
    try {
      message = JSON.parse(e.response.body)
    } catch (e) {
      message = 'exchangeForTokens error'
    }

    throw new Error(message)
  }
}

_.exchangeForTokens = exchangeForTokens

const refreshAccessToken = async (refreshToken) => {
  const refreshTokenProof = {
    grant_type: 'refresh_token',
    client_id: CLIENT_ID,
    client_secret: CLIENT_SECRET,
    redirect_uri: getRedirectUrl(),
    refresh_token: refreshToken
  }

  return exchangeForTokens(refreshTokenProof)
}

_.getAccessToken = async (hubDoc, force = false) => {
  if (!hubDoc.exists) {
    throw new Error('hubDoc does not exist')
  }

  let { refresh_token, access_token, expires_in } = hubDoc.data()

  if (force || expires_in < Date.now() - 3600 * 1000) {
    ({ refresh_token, access_token, expires_in } = await refreshAccessToken(refresh_token))
    const dataToUpdate = {
      refresh_token: refresh_token || null,
      access_token: access_token || null,
      expires_in: expires_in || null
    }
    await hubDoc.ref.update(dataToUpdate)
  }
  return access_token
}

_.required = required

module.exports = _
