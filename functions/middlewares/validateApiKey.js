const HTTP_STATUS = require('http-status')
const db = require('firebase-admin').firestore()
var utils = require('../lib/utils')

module.exports = async (req, res, next) => {
  let api_key = req.query.api_key
  if (!api_key) {
    api_key = req.body.api_key
  }
  const errorResponse = {
    success: false,
    error_message: `Invalid API key`,
    error_code: 'error/unauthorised'
  }
  let user = null

  if (!api_key) {
    return res.status(HTTP_STATUS.FORBIDDEN).json(errorResponse)
  }

  const uid = api_key.split('$')[0]
  try {
    user = await utils.get_user_profile(uid)
  } catch (error) {
    return res.status(HTTP_STATUS.FORBIDDEN).json(errorResponse)
  }

  if (user.api_key === api_key) {
    req.locals = {
      user: user,
      uid: uid
    }

    return next()
  }

  return res.status(HTTP_STATUS.FORBIDDEN).json(errorResponse)
}
