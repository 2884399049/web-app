const constants = require('./constants')

function calculate_lead_time (printer, campaign, recipients) {
  let _recipients = recipients
  if (typeof recipients === 'object') {
    _recipients = JSON.stringify(recipients)
  }

  let _campaign = campaign
  if (typeof campaign === 'object') {
    _campaign = JSON.stringify(campaign)
  }

  let code = `
  let recipients = ${_recipients}
  let campaign = ${_campaign}
  let constants = ${JSON.stringify(constants)}
  ${printer.lead_time_code}

  main(campaign, recipients)
  `

  let result = {code_output: undefined, code_error: undefined}

  let out = ''
  try {
    out = eval(code)
    result.code_output = out
  } catch (err) {
    result.code_error = err
  }

  return result
}

module.exports = {
  calculate_lead_time
}
