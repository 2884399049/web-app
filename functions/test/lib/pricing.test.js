const projectId = 'pricing-test'
process.env.GCLOUD_PROJECT = projectId
process.env.FIRESTORE_EMULATOR_HOST = 'localhost:8080'

const firebase_test = require('firebase-functions-test')()
const firebase = require('@firebase/testing')

firebase_test.mockConfig({ slack: {api_key: '', channels: {}}, stripe: { key: '' }, sendgrid: {apikey: ''}, hubspot: {api_key: '', enabled: false} });

const admin = require('firebase-admin')

admin.initializeApp({ projectId })

const db = admin.firestore()

jest.mock("stripe", () => {
  return jest.fn().mockImplementation(() => {
    return {
      invoiceItems: {
        create: () => {},
      },
      invoices: {
        create: () => ({id: '21'}),
        finalizeInvoice: () => ({id: '21', total: 1, tax: 2, amount_due: 3, starting_balance: 4, ending_balance: 5}),
      },
    }
  })
})

const constants = require('../../shared/constants')
const pricing_shared = require('../../shared/pricing')
const pricing = require('functions/lib/pricing.js')
const Hubspot = require('functions/lib/hubspot-utils')

const doc_product_full_service_a5 = {
  goods: [constants.GOOD.full_service_a5],
  tiers: {
    'usd': {
      1: 500,
      10: 400,
      100: 300
    },
    'gbp': {
      1: 501,
      10: 401,
      100: 301
    }
  }
}

const doc_product_full_service_a4 = {
  goods: [constants.GOOD.full_service_a4],
  tiers: {
    'usd': {
      1: 600,
      10: 500,
      100: 400
    },
    'gbp': {
      1: 601,
      10: 501,
      100: 401
    }
  }
}

const doc_product_full_service_a4_override = {
  goods: [constants.GOOD.full_service_a4],
  tiers: {
    'usd': {
      1: 600,
      2: 500,
      3: 400
    },
    'gbp': {
      1: 601,
      2: 501,
      3: 401
    }
  }
}

describe('goods_to_stamps', () => {
  it('No stamps for Self Service', () => {
    const mock_campaign = {product: constants.PRODUCT.self_service, envelope: {required: constants.ENVELOPE_REQUIRED.yes, stamp: constants.STAMP_TYPES.first_class}}
    const mock_recipients = [{country: 'GB'}]
    let mock_goods = {}

    pricing.goods_to_stamps(mock_campaign, mock_recipients, mock_goods)
    expect(mock_goods).toEqual({})
  })

  it('No stamps for no envelope', () => {
    const mock_campaign = {product: constants.PRODUCT.full_service, envelope: {required: constants.ENVELOPE_REQUIRED.no, stamp: constants.STAMP_TYPES.first_class}}
    const mock_recipients = [{country: 'GB'}]
    let mock_goods = {}

    pricing.goods_to_stamps(mock_campaign, mock_recipients, mock_goods)
    expect(mock_goods).toEqual({})
  })

  it('No stamps for no stamp', () => {
    const mock_campaign = {product: constants.PRODUCT.full_service, envelope: {required: constants.ENVELOPE_REQUIRED.yes, stamp: constants.STAMP_TYPES.no_stamp}}
    const mock_recipients = [{country: 'GB'}]
    let mock_goods = {}

    pricing.goods_to_stamps(mock_campaign, mock_recipients, mock_goods)
    expect(mock_goods).toEqual({})
  })

  it('Stamp countries', () => {
    const mock_campaign = {product: constants.PRODUCT.full_service, envelope: {required: constants.ENVELOPE_REQUIRED.yes, stamp: constants.STAMP_TYPES.first_class}}
    const mock_recipients = [{country: 'GB'}, {country: 'GB'}, {country: 'US'}, {country: 'US'}, {country: 'MX'}, {country: 'AL'}, {country: 'AU'}, {country: 'AF'}]
    let mock_goods = {}

    let expected_goods = {}
    expected_goods[constants.GOOD.stamp_gb_first_class] = 2
    expected_goods[constants.GOOD.stamp_us_first_class] = 2
    expected_goods[constants.GOOD.stamp_us_international] = 4
    // expected_goods[constants.GOOD.stamp_gb_europe] = 0
    // expected_goods[constants.GOOD.stamp_gb_world_zone_1] = 1
    // expected_goods[constants.GOOD.stamp_gb_world_zone_2] = 1

    pricing.goods_to_stamps(mock_campaign, mock_recipients, mock_goods)

    expect(mock_goods).toEqual(expected_goods)
  })
})

describe('campaign_to_goods', () => {
  it('Self Service campaign with no envelope and no stationery to goods', () => {
    const mock_campaign = {product: constants.PRODUCT.self_service, stationary_required: false, envelope: {required: constants.ENVELOPE_REQUIRED.no, stamp: constants.STAMP_TYPES.first_class}}
    const mock_recipients = [{country: 'GB'}]
    const mock_stationery = {paper: {name: 'A4'}}

    let expected_goods = {}

    let goods = pricing.campaign_to_goods(mock_campaign, mock_recipients, mock_stationery)

    expect(goods).toEqual(expected_goods)
  })

  it('Self Service campaign with envelope and no stationery to goods', () => {
    const mock_campaign = {product: constants.PRODUCT.self_service, stationary_required: false, envelope: {required: constants.ENVELOPE_REQUIRED.yes, stamp: constants.STAMP_TYPES.no_stamp, paper: {name: 'DL'}}}
    const mock_recipients = [{country: 'GB'}]
    const mock_stationery = {paper: {name: 'A4'}}

    let expected_goods = {}
    expected_goods[constants.GOOD.self_service_dl] = 1

    let goods = pricing.campaign_to_goods(mock_campaign, mock_recipients, mock_stationery)

    expect(goods).toEqual(expected_goods)
  })

  it('Self Service campaign with envelope and stationery to goods', () => {
    const mock_campaign = {product: constants.PRODUCT.self_service, stationary_required: true, envelope: {required: constants.ENVELOPE_REQUIRED.yes, stamp: constants.STAMP_TYPES.no_stamp, paper: {name: 'DL'}}}
    const mock_recipients = [{country: 'GB'}, {country: 'GB'}]
    const mock_stationery = {paper: {name: 'A4'}}

    let expected_goods = {}
    expected_goods[constants.GOOD.self_service_dl] = 2
    expected_goods[constants.GOOD.self_service_a4] = 2

    let goods = pricing.campaign_to_goods(mock_campaign, mock_recipients, mock_stationery)

    expect(goods).toEqual(expected_goods)
  })

  it('Full Service campaign with envelope and stationery to goods', () => {
    const mock_campaign = {product: constants.PRODUCT.full_service, stationary_required: true, envelope: {required: constants.ENVELOPE_REQUIRED.yes, stamp: constants.STAMP_TYPES.first_class, paper: {name: 'C5'}}}
    const mock_recipients = [{country: 'GB'}, {country: 'US'}]
    const mock_stationery = {paper: {name: 'A5'}}

    let expected_goods = {}
    expected_goods[constants.GOOD.full_service_a5_pack] = 2
    expected_goods[constants.GOOD.stamp_gb_first_class] = 1
    expected_goods[constants.GOOD.stamp_us_first_class] = 1

    let goods = pricing.campaign_to_goods(mock_campaign, mock_recipients, mock_stationery)

    expect(goods).toEqual(expected_goods)
  })

  it('Full Service campaign with no envelope and stationery to goods', () => {
    const mock_campaign = {product: constants.PRODUCT.full_service, stationary_required: true, envelope: {required: constants.ENVELOPE_REQUIRED.no, stamp: constants.STAMP_TYPES.first_class, paper: {name: 'C5'}}}
    const mock_recipients = [{country: 'GB'}, {country: 'US'}]
    const mock_stationery = {paper: {name: 'A5'}}

    let expected_goods = {}
    expected_goods[constants.GOOD.full_service_a5] = 2

    let goods = pricing.campaign_to_goods(mock_campaign, mock_recipients, mock_stationery)

    expect(goods).toEqual(expected_goods)
  })

  it('Full Service campaign with envelope and no stationery to goods', () => {
    const mock_campaign = {product: constants.PRODUCT.full_service, stationary_required: false, envelope: {required: constants.ENVELOPE_REQUIRED.yes, stamp: constants.STAMP_TYPES.first_class, paper: {name: 'C5'}}}
    const mock_recipients = [{country: 'GB'}, {country: 'US'}]
    const mock_stationery = {paper: {name: 'A5'}}

    let expected_goods = {}
    expected_goods[constants.GOOD.full_service_c5] = 2
    expected_goods[constants.GOOD.stamp_gb_first_class] = 1
    expected_goods[constants.GOOD.stamp_us_first_class] = 1

    let goods = pricing.campaign_to_goods(mock_campaign, mock_recipients, mock_stationery)

    expect(goods).toEqual(expected_goods)
  })

  it('Full Service campaign with ship to self to goods', () => {
    const mock_campaign = {product: constants.PRODUCT.full_service, delivery: {sender: constants.SENDER.ship_to_self}, stationary_required: true, envelope: {required: constants.ENVELOPE_REQUIRED.no, stamp: constants.STAMP_TYPES.first_class, paper: {name: 'C5'}}}

    let mock_recipients = []
    for (let i=0; i<4500; i++) {
      mock_recipients.push({country: 'GB'})
    }
    const mock_stationery = {paper: {name: 'A5'}}

    let expected_goods = {}
    expected_goods[constants.GOOD.full_service_a5] = 4500
    expected_goods[constants.GOOD.ship_to_self_gb] = 5

    let goods = pricing.campaign_to_goods(mock_campaign, mock_recipients, mock_stationery)

    expect(goods).toEqual(expected_goods)
  })
})

describe('price_good', () => {
  it('Price good', () => {
    let mock_price_table = {}
    mock_price_table[constants.GOOD.full_service_a5] = {
      goods: [constants.GOOD.full_service_a5],
      tiers: {
        'usd': {
          1: 500,
          10: 400,
          100: 300
        }
      }
    }

    expect(pricing.price_good('usd', mock_price_table, constants.GOOD.full_service_a5, 1, 1)).toEqual(500)
    expect(pricing.price_good('usd', mock_price_table, constants.GOOD.full_service_a5, 9, 9)).toEqual(400 * 9)
    expect(pricing.price_good('usd', mock_price_table, constants.GOOD.full_service_a5, 11, 11)).toEqual(300 * 11)
    expect(pricing.price_good('usd', mock_price_table, constants.GOOD.full_service_a5, 110, 110)).toEqual(300 * 110)
  })
})

describe('build_price_table', () => {
  beforeEach(async () => {
    await firebase.clearFirestoreData({ projectId });
  })

  it('Builds correct table with no user overrides', async () => {
    await db.collection('products').doc(constants.GOOD.full_service_a5).set(doc_product_full_service_a5)
    await db.collection('products').doc(constants.GOOD.full_service_a4).set(doc_product_full_service_a4)

    let expected_price_table = {}
    expected_price_table[constants.GOOD.full_service_a5] = doc_product_full_service_a5
    expected_price_table[constants.GOOD.full_service_a4] = doc_product_full_service_a4

    let mock_user = {
      id: '123'
    }

    expect(await pricing.build_price_table(mock_user)).toEqual(expected_price_table)
  })

  it('builds correct table with user overrides', async () => {
    await db.collection('products').doc(constants.GOOD.full_service_a5).set(doc_product_full_service_a5)
    await db.collection('products').doc(constants.GOOD.full_service_a4).set(doc_product_full_service_a4)
    await db.collection('users').doc('123').collection('products').doc(constants.GOOD.full_service_a4).set(doc_product_full_service_a4_override)

    let expected_price_table = {}
    expected_price_table[constants.GOOD.full_service_a5] = doc_product_full_service_a5
    expected_price_table[constants.GOOD.full_service_a4] = doc_product_full_service_a4_override

    let mock_user = {
      id: '123'
    }

    expect(await pricing.build_price_table(mock_user)).toEqual(expected_price_table)
  })
})

describe('caluclate_price', () => {
  beforeEach(async () => {
    await firebase.clearFirestoreData({ projectId });
  })

  it('prices goods', async () => {
    await db.collection('products').doc(constants.GOOD.full_service_a5).set(doc_product_full_service_a5)
    await db.collection('products').doc(constants.GOOD.full_service_a4).set(doc_product_full_service_a4)
    await db.collection('users').doc('123').collection('products').doc(constants.GOOD.full_service_a4).set(doc_product_full_service_a4_override)

    let goods = {}
    goods[constants.GOOD.full_service_a5] = 3
    goods[constants.GOOD.full_service_a4] = 5

    let expected_price = {total: 5 * 400 + 3 * 400, currency: 'usd'}

    let mock_user = {
      id: '123',
      currency: 'usd'
    }

    let price_table = await pricing.build_price_table(mock_user)
    const got = await pricing.caluclate_price(price_table, mock_user, goods, 5)
    expect(got.total).toEqual(expected_price.total)
    expect(got.currency).toEqual(expected_price.currency)
  })

  it('correctly calculates credit', async () => {
    await db.collection('products').doc(constants.GOOD.full_service_a5).set(doc_product_full_service_a5)
    await db.collection('products').doc(constants.GOOD.full_service_a4).set(doc_product_full_service_a4)
    await db.collection('users').doc('123').collection('products').doc(constants.GOOD.full_service_a4).set(doc_product_full_service_a4_override)

    let goods = {}
    goods[constants.GOOD.full_service_a5] = 3
    goods[constants.GOOD.full_service_a4] = 5
    goods[constants.GOOD.membership] = 10

    let mock_user = {
      id: '123',
      currency: 'usd',
      credit: 4000
    }

    let expected_price = {total: 10, currency: 'usd', credit_left: mock_user.credit - (5 * 400 + 3 * 400)}

    let price_table = await pricing.build_price_table(mock_user)
    const got = await pricing.caluclate_price(price_table, mock_user, goods, 5, mock_user.credit)
    expect(got.credit_left).toEqual(expected_price.credit_left)
    expect(got.total).toEqual(expected_price.total)
    expect(got.currency).toEqual(expected_price.currency)
  })
})

describe('aggregate_usage_records', () => {
  beforeEach(async () => {
    await firebase.clearFirestoreData({ projectId });
  })

  const mock_user_1 = {
    id: '1',
    currency: 'usd',
    billing_period_start_date: new Date()
  }

  const mock_user_2 = {
    id: '2',
    currency: 'gbp',
    billing_period_start_date: new Date()
  }

  let mock_goods_1 = {}
  mock_goods_1[constants.GOOD.full_service_a5] = 3
  mock_goods_1[constants.GOOD.full_service_a4] = 5

  let mock_goods_2 = {}
  mock_goods_2[constants.GOOD.full_service_a5] = 7

  let mock_goods_3 = {}
  mock_goods_3[constants.GOOD.full_service_a5] = 3

  let mock_goods_4 = {}
  mock_goods_4[constants.GOOD.full_service_a4] = -1

  it('aggregates multiple usage records', async () => {
    await firebase.clearFirestoreData({ projectId })

    await pricing.add_usage_record(mock_user_1, mock_goods_3, 1)

    // Mark this record as already charged
    let usage_docs = await db.collection('usage').get()
    for (let usage_doc of usage_docs.docs) {
      await usage_doc.ref.update({invoice_doc: true})
    }

    await pricing.add_usage_record(mock_user_1, mock_goods_1, 1)
    await pricing.add_usage_record(mock_user_1, mock_goods_2, 1)
    await pricing.add_usage_record(mock_user_1, mock_goods_4, 1)
    await pricing.add_usage_record(mock_user_2, mock_goods_1, 1)

    let expected_usage = {}
    expected_usage[constants.GOOD.full_service_a5] = 10
    expected_usage[constants.GOOD.full_service_a4] = 4

    let aggregate_usage = await pricing.aggregate_usage_records(mock_user_1)
    expect(aggregate_usage.goods).toEqual(expected_usage)
  })
})

describe('start_new_billing_period', () => {
  beforeEach(async () => {
    await firebase.clearFirestoreData({ projectId });
  })

  const mock_user_1 = {
    id: '1',
    currency: 'usd',
    billing_period_start_date: '1',
    billing_period_end_date: '2',
    billing_period_recipients_count: 2
  }

  it('creates invoice and updates billing period stats', async () => {
    await db.collection('users').doc(mock_user_1.id).set(mock_user_1)

    await pricing.start_new_billing_period(mock_user_1)

    // Mark this record as already charged
    let user_doc = await db.collection('users').doc(mock_user_1.id).get()
    let user = user_doc.data()

    expect(user.billing_period_recipients_count).toEqual(0)

    let invoice_docs = await db.collection('invoices').get()
    expect(invoice_docs.docs.length).toEqual(1)

    let invoice
    for (let invoice_doc of invoice_docs.docs) {
      invoice = invoice_doc.data()
    }

    expect(invoice.status).toEqual(constants.INVOICE_STATUS.draft)
    expect(invoice.billing_from).toEqual(user.billing_period_start_date)
    expect(invoice.billing_to).toEqual(user.billing_period_end_date)
  })
})

describe('finalise_invoice', () => {
  const mock_user_1 = {
    id: '1',
    currency: 'usd',
    billing_period_start_date: '1',
    billing_period_end_date: '2',
    billing_period_recipients_count: 2,
    credit: 0
  }

  const mock_user_1_credit = {
    id: '1',
    currency: 'usd',
    billing_period_start_date: '1',
    billing_period_end_date: '2',
    billing_period_recipients_count: 2,
    credit_subscription: 500,
    credit: 150
  }

  const mock_goods = {}
  mock_goods[constants.GOOD.full_service_a4] = 2

  const mock_invoice_draft = {
    id: '1',
    status: constants.INVOICE_STATUS.draft,
    created: new Date(),
    billing_from: mock_user_1.billing_period_start_date,
    billing_to: mock_user_1.billing_period_end_date,
    uid: mock_user_1.id,
    amount: 2,
    amount_gbp: 2,
    recipients_count: 2,
    goods: mock_goods
  }

  const mock_invoice_paid = {
    id: '2',
    status: constants.INVOICE_STATUS.paid,
    created: new Date(),
    billing_from: mock_user_1.billing_period_start_date,
    billing_to: mock_user_1.billing_period_end_date,
    uid: mock_user_1.id,
    amount: 3,
    amount_gbp: 3,
    recipients_count: 2,
    goods: mock_goods
  }

  beforeEach(async () => {
    await firebase.clearFirestoreData({ projectId })

    await pricing.add_usage_record(mock_user_1, mock_goods, 2)

    await db.collection('invoices').doc(mock_invoice_draft.id).set(mock_invoice_draft)
    await db.collection('invoices').doc(mock_invoice_paid.id).set(mock_invoice_paid)
  })

  it('Adds subscription credit', async () => {
    await db.collection('users').doc(mock_user_1_credit.id).set(mock_user_1_credit)

    pricing.build_price_table = jest.fn().mockReturnValue({})
    pricing_shared.caluclate_price = jest.fn().mockReturnValue({credit_left: 200, total: 400, total_gbp: 400, currency: 'gbp'})

    Hubspot.update_deal_for_invoice = jest.fn().mockReturnValue({})

    await pricing.finalise_invoice(mock_user_1_credit, mock_invoice_draft)

    let user_doc = await db.collection('users').doc(mock_user_1_credit.id).get()
    let user = user_doc.data()

    let invoice_doc = await db.collection('invoices').doc(mock_invoice_draft.id).get()
    let invoice = invoice_doc.data()

    let usage_docs = await db.collection('usage').get()
    for (let usage_doc of usage_docs.docs) {
      let usage = usage_doc.data()
      expect(usage.invoice_doc).toEqual(db.collection('invoices').doc(mock_invoice_draft.id))
    }

    expect(invoice.status).toBe(constants.INVOICE_STATUS.open)

    expect(invoice.credit_used).toBe((mock_user_1_credit.credit + mock_user_1_credit.credit_subscription) - 200)
    expect(invoice.goods[constants.GOOD.membership]).toBe(mock_user_1_credit.credit_subscription)
    expect(user.credit).toBe(200)

    expect(Hubspot.update_deal_for_invoice).toBeCalled()
  })

  it('Dosen\'t invoice stripe if no charge', async () => {
    await db.collection('users').doc(mock_user_1_credit.id).set(mock_user_1_credit)

    pricing.build_price_table = jest.fn().mockReturnValue({})
    pricing_shared.caluclate_price = jest.fn().mockReturnValue({credit_left: 200, total: 0, total_gbp: 0, currency: 'gbp'})

    Hubspot.update_deal_for_invoice = jest.fn().mockReturnValue({})

    await pricing.finalise_invoice(mock_user_1_credit, mock_invoice_draft)

    let invoice_doc = await db.collection('invoices').doc(mock_invoice_draft.id).get()
    let invoice = invoice_doc.data()

    expect(invoice.status).toBe(constants.INVOICE_STATUS.paid)
  })
})

firebase_test.cleanup()
