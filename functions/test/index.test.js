const projectId = 'route-v1-functions-test'
process.env.GCLOUD_PROJECT = projectId
process.env.FIRESTORE_EMULATOR_HOST = 'localhost:8080'

const firebase_test = require('firebase-functions-test')()
const firebase = require('@firebase/testing')

firebase_test.mockConfig({ slack: {api_key: '', channels: {}}, stripe: { key: '' }, sendgrid: {apikey: ''}, hubspot: {api_key: '', enabled: false}, firestore: {storagebucket: '123'}, shopify: {serviceAddress: '1'} });

const admin = require('firebase-admin')

admin.initializeApp({ projectId })

const db = admin.firestore()

jest.mock('request-promise-native')

const constants = require('../shared/constants')
const functions = require('functions/index.js')

// firestore emulator cannot handle timestamps, so mock them out
const admin_mock = require('firebase-admin');
admin_mock.firestore.Timestamp.now = jest.fn().mockReturnValue('date mock')

describe('recipient_updated', () => {
  beforeEach(async () => {
    await firebase.clearFirestoreData({ projectId });
  })

  const campaign_id = '1'

  const context = {
    params: {
      campaign_id: campaign_id,
      recipient_id: '1'
    }
  }

  // Firestore emulation dosen't currently test increment operator, so skip test for now
  it.skip('recipient status changed', async () => {
    const api_v1_functions = require('functions/routes/v1/functions')
    api_v1_functions.toggle_recipient_deleted = jest.fn()

    let mock_campaign = {recipients_count: 1, recipients_count_by_status: {}}
    mock_campaign['recipients_count_by_status'][constants.RECIPIENT_STATUS.pending] = 1

    await db.collection('campaigns').doc(campaign_id).set(mock_campaign)

    const wrapped = firebase_test.wrap(functions.recipient_updated)

    const change = {
      before: {
        data: () => ({status: constants.RECIPIENT_STATUS.pending, testing: false})
      },
      after: {
        data: () => ({status: constants.RECIPIENT_STATUS.in_progress, testing: false})
      }
    }

    await wrapped(change, context)

    expect(api_v1_functions.toggle_recipient_deleted).toHaveBeenCalledTimes(0);

    let expected_recipients_count_by_status = {}
    expected_recipients_count_by_status[constants.RECIPIENT_STATUS.pending] = 0
    expected_recipients_count_by_status[constants.RECIPIENT_STATUS.in_progress] = 1
    expected_recipients_count_by_status[constants.RECIPIENT_STATUS.ready_to_print] = 0
    expected_recipients_count_by_status[constants.RECIPIENT_STATUS.shipped] = 0
    expected_recipients_count_by_status[constants.RECIPIENT_STATUS.error] = 0
    expected_recipients_count_by_status[constants.RECIPIENT_STATUS.deleted] = 0

    let campaign_doc = await db.collection('campaigns').doc(campaign_id).get()
    expect(campaign_doc.data().recipients_count).toEqual(1)
    expect(campaign_doc.data().recipients_count_by_status).toEqual(expected_recipients_count_by_status)
  })

  // Firestore emulation dosen't currently test increment operator, so skip test for now
  it.skip('recipient status changed to delete', async () => {
    const api_v1_functions = require('functions/routes/v1/functions')
    api_v1_functions.toggle_recipient_deleted = jest.fn()

    let mock_campaign = {recipients_count: 1, recipients_count_by_status: {}}
    mock_campaign['recipients_count_by_status'][constants.RECIPIENT_STATUS.pending] = 1

    await db.collection('campaigns').doc(campaign_id).set(mock_campaign)

    const wrapped = firebase_test.wrap(functions.recipient_updated)

    const change = {
      before: {
        data: () => ({status: constants.RECIPIENT_STATUS.pending, testing: false})
      },
      after: {
        data: () => ({status: constants.RECIPIENT_STATUS.deleted, testing: false})
      }
    }

    await wrapped(change, context)

    expect(api_v1_functions.toggle_recipient_deleted).toHaveBeenCalledTimes(1)

    let expected_recipients_count_by_status = {}
    expected_recipients_count_by_status[constants.RECIPIENT_STATUS.pending] = 0
    expected_recipients_count_by_status[constants.RECIPIENT_STATUS.in_progress] = 0
    expected_recipients_count_by_status[constants.RECIPIENT_STATUS.ready_to_print] = 0
    expected_recipients_count_by_status[constants.RECIPIENT_STATUS.shipped] = 0
    expected_recipients_count_by_status[constants.RECIPIENT_STATUS.error] = 0
    expected_recipients_count_by_status[constants.RECIPIENT_STATUS.deleted] = 1

    let campaign_doc = await db.collection('campaigns').doc(campaign_id).get()
    expect(campaign_doc.data().recipients_count).toEqual(0)
    expect(campaign_doc.data().recipients_count_by_status).toEqual(expected_recipients_count_by_status)
  })

  // Firestore emulation dosen't currently test increment operator, so skip test for now
  it.skip('recipient status changed to testing', async () => {
    const api_v1_functions = require('functions/routes/v1/functions')
    api_v1_functions.toggle_recipient_deleted = jest.fn()

    let mock_campaign = {recipients_count: 1, recipients_count_by_status: {}}
    mock_campaign['recipients_count_by_status'][constants.RECIPIENT_STATUS.pending] = 1

    await db.collection('campaigns').doc(campaign_id).set(mock_campaign)

    const wrapped = firebase_test.wrap(functions.recipient_updated)

    const change = {
      before: {
        data: () => ({status: constants.RECIPIENT_STATUS.pending, testing: false})
      },
      after: {
        data: () => ({status: constants.RECIPIENT_STATUS.pending, testing: true})
      }
    }

    await wrapped(change, context)

    expect(api_v1_functions.toggle_recipient_deleted).toHaveBeenCalledTimes(1)

    let expected_recipients_count_by_status = {}
    expected_recipients_count_by_status[constants.RECIPIENT_STATUS.pending] = 1
    expected_recipients_count_by_status[constants.RECIPIENT_STATUS.in_progress] = 0
    expected_recipients_count_by_status[constants.RECIPIENT_STATUS.ready_to_print] = 0
    expected_recipients_count_by_status[constants.RECIPIENT_STATUS.shipped] = 0
    expected_recipients_count_by_status[constants.RECIPIENT_STATUS.error] = 0
    expected_recipients_count_by_status[constants.RECIPIENT_STATUS.deleted] = 0

    let campaign_doc = await db.collection('campaigns').doc(campaign_id).get()
    expect(campaign_doc.data().recipients_count).toEqual(1)
    expect(campaign_doc.data().recipients_count_by_status).toEqual(expected_recipients_count_by_status)
  })
})

firebase_test.cleanup()
