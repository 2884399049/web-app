const projectId = 'route-v1-functions-test'
process.env.GCLOUD_PROJECT = projectId
process.env.FIRESTORE_EMULATOR_HOST = 'localhost:8080'

const firebase_test = require('firebase-functions-test')()
const firebase = require('@firebase/testing')

firebase_test.mockConfig({ slack: {api_key: '', channels: {}}, stripe: { key: '' }, sendgrid: {apikey: ''}, hubspot: {api_key: '', enabled: false}, firestore: {storagebucket: '123'} });

const admin = require('firebase-admin')

admin.initializeApp({ projectId })

const db = admin.firestore()

const constants = require('../../../shared/constants')
const api_v1_functions = require('functions/routes/v1/functions')

// firestore emulator cannot handle timestamps, so mock them out
const admin_mock = require('firebase-admin');
admin_mock.firestore.Timestamp.now = jest.fn().mockReturnValue('date mock')

describe('toggle_recipient_deleted', () => {
  beforeEach(async () => {
    await firebase.clearFirestoreData({ projectId });
  })

  const mock_recipient = {
    id: '1',
    country: 'GB',
    status: constants.RECIPIENT_STATUS.pending,
    testing: false
  }

  const mock_recipient_testing = {
    id: '1',
    country: 'GB',
    status: constants.RECIPIENT_STATUS.pending,
    testing: true
  }

  const mock_recipient_deleted = {
    id: '1',
    country: 'GB',
    status: constants.RECIPIENT_STATUS.deleted,
    testing: true
  }

  const mock_campaign = {
    uid: '1',
    stationary: '1',
    stationary_required: true,
    is_sample: false,
    product: constants.PRODUCT.full_service,
    envelope: {
      required: constants.ENVELOPE_REQUIRED.yes,
      stamp: constants.STAMP_TYPES.first_class,
      paper: {name: 'C5'}
    }
  }

  const mock_campaign_sample = {
    uid: '1',
    stationary: '1',
    stationary_required: true,
    is_sample: true,
    product: constants.PRODUCT.full_service,
    envelope: {
      required: constants.ENVELOPE_REQUIRED.yes,
      stamp: constants.STAMP_TYPES.first_class,
      paper: {name: 'C5'}
    }
  }

  const mock_user = {
    id: '1',
    currency: 'usd',
    billing_period_start_date: new Date()
  }

  const mock_stationery = {
    paper: {name: 'A5'}
  }

  const campaign_id = '1'

  it('Adds usage when recipient added', async () => {
    await db.collection('users').doc(mock_user.id).set(mock_user)
    await db.collection('campaigns').doc(campaign_id).set(mock_campaign)
    await db.collection('stationary').doc(mock_campaign.stationary).set(mock_stationery)

    const expected_goods = {}
    expected_goods[constants.GOOD.full_service_a5_pack] = 1
    expected_goods[constants.GOOD.stamp_gb_first_class] = 1

    await api_v1_functions.toggle_recipient_deleted(mock_recipient, campaign_id)

    let usage_docs = await db.collection('usage').get()
    for (let usage_doc of usage_docs.docs) {
      expect(usage_doc.data().goods).toEqual(expected_goods)
    }
  })

  it('No usage when sample campaign', async () => {
    await db.collection('users').doc(mock_user.id).set(mock_user)
    await db.collection('campaigns').doc(campaign_id).set(mock_campaign_sample)
    await db.collection('stationary').doc(mock_campaign_sample.stationary).set(mock_stationery)

    const expected_goods = {}

    await api_v1_functions.toggle_recipient_deleted(mock_recipient, campaign_id)

    let usage_docs = await db.collection('usage').get()
    for (let usage_doc of usage_docs.docs) {
      expect(usage_doc.data().goods).toEqual(expected_goods)
    }
  })

  it('Negative usage when testing recipient', async () => {
    await db.collection('users').doc(mock_user.id).set(mock_user)
    await db.collection('campaigns').doc(campaign_id).set(mock_campaign_sample)
    await db.collection('stationary').doc(mock_campaign.stationary).set(mock_stationery)

    const expected_goods = {}
    expected_goods[constants.GOOD.full_service_a5] = -1
    expected_goods[constants.GOOD.full_service_c5] = -1
    expected_goods[constants.GOOD.stamp_gb_first_class] = -1

    await api_v1_functions.toggle_recipient_deleted(mock_recipient_testing, campaign_id)
    await api_v1_functions.toggle_recipient_deleted(mock_recipient_deleted, campaign_id)

    let usage_docs = await db.collection('usage').get()
    for (let usage_doc of usage_docs.docs) {
      expect(usage_doc.data().goods).toEqual(expected_goods)
    }
  })
})

describe('add_recipients', () => {
  beforeEach(async () => {
    await firebase.clearFirestoreData({ projectId });
  })

  const mock_recipient_invalid = {}

  const mock_recipient = {'last name': 'mock recipient'}
  const mock_recipient_1 = {'last name': 'mock recipient 1'}
  const mock_recipient_2 = {'last name': 'mock recipient 2'}
  const mock_recipient_3 = {'last name': 'mock recipient 3'}
  const mock_recipient_4 = {'last name': 'mock recipient 4'}
  const mock_recipient_5 = {'last name': 'mock recipient 5'}

  let mock_campaign = {
    uid: '1',
    stationary: '1',
    stationary_required: true,
    is_sample: false,
    product: constants.PRODUCT.full_service,
    envelope: {
      required: constants.ENVELOPE_REQUIRED.yes,
      stamp: constants.STAMP_TYPES.first_class,
      paper: {name: 'C5'}
    }
  }

  let mock_campaign_testing = {...mock_campaign}
  mock_campaign_testing.testing = true

  let mock_campaign_sample = {...mock_campaign}
  mock_campaign_sample.is_sample = true

  let mock_campaign_sample_already_has_recipients = {...mock_campaign}
  mock_campaign_sample_already_has_recipients.is_sample = true
  mock_campaign_sample_already_has_recipients.recipients_count = 1

  const mock_user = {
    id: '1',
    currency: 'usd',
    billing_period_start_date: new Date()
  }

  const mock_stationery = {
    paper: {name: 'A5'}
  }

  const campaign_id = '1'

  it('Throws error for invalid recipient', async () => {
    let result = await api_v1_functions.add_recipients(campaign_id, [mock_recipient_invalid, mock_recipient, mock_recipient], mock_user.id)
    expect(result.error_code).toEqual('error/invalid-key')
  })

  it('Throws error sample campaign and no usage', async () => {
    await db.collection('users').doc(mock_user.id).set(mock_user)
    await db.collection('campaigns').doc(campaign_id).set(mock_campaign_sample_already_has_recipients)
    await db.collection('stationary').doc(mock_campaign.stationary).set(mock_stationery)

    let result = await api_v1_functions.add_recipients(campaign_id, [mock_recipient], mock_user.id)
    expect(result.error_code).toEqual('error/sample-campaign')

    await db.collection('campaigns').doc(campaign_id).set(mock_campaign_sample)

    result = await api_v1_functions.add_recipients(campaign_id, [mock_recipient, mock_recipient], mock_user.id)
    expect(result.error_code).toEqual('error/sample-campaign')

    result = await api_v1_functions.add_recipients(campaign_id, [mock_recipient], mock_user.id)
    expect(result.success).toEqual(true)
    expect((await db.collection('usage').get()).docs.length).toEqual(0)
  })

  it('Sets recipients as testing and no usage records', async () => {
    await db.collection('users').doc(mock_user.id).set(mock_user)
    await db.collection('campaigns').doc(campaign_id).set(mock_campaign_testing)
    await db.collection('stationary').doc(mock_campaign.stationary).set(mock_stationery)

    let result = await api_v1_functions.add_recipients(campaign_id, [mock_recipient, mock_recipient], mock_user.id)

    let recipient_docs = await db.collection('campaigns').doc(campaign_id).collection('recipients').get()
    for (let recipient_doc of recipient_docs.docs) {
      expect(recipient_doc.data().testing).toEqual(true)
    }

   expect((await db.collection('usage').get()).docs.length).toEqual(0)
  })

  it('Sets recipients with correct index and usage', async () => {
    await db.collection('users').doc(mock_user.id).set(mock_user)
    await db.collection('campaigns').doc(campaign_id).set(mock_campaign)
    await db.collection('stationary').doc(mock_campaign.stationary).set(mock_stationery)

    let result = await api_v1_functions.add_recipients(campaign_id, [mock_recipient_1], mock_user.id)
    expect((await db.collection('campaigns').doc(campaign_id).collection('recipients').get()).docs.length).toEqual(1)
    expect((await db.collection('usage').get()).docs.length).toEqual(1)

    result = await api_v1_functions.add_recipients(campaign_id, [mock_recipient_2, mock_recipient_3, mock_recipient_4], mock_user.id)
    expect((await db.collection('campaigns').doc(campaign_id).collection('recipients').get()).docs.length).toEqual(4)
    expect((await db.collection('usage').get()).docs.length).toEqual(2)

    result = await api_v1_functions.add_recipients(campaign_id, [mock_recipient_5], mock_user.id)
    expect((await db.collection('campaigns').doc(campaign_id).collection('recipients').get()).docs.length).toEqual(5)
    expect((await db.collection('usage').get()).docs.length).toEqual(3)
  })
})

firebase_test.cleanup()
