const index = require("../../hubspot/functions/index");

//Params
test("Refactors contact into address", () => {
	const contact = {
		properties: {
			firstname: {
				value: "Kelvin"
			},
			lastname: {
				value: "Cornelissens"
			},
			address: {
				value: "Lantmanstraat 2"
			},
			country: {
				value: "The Netherlands"
			},
			state: {
				value: "Overijssel"
			},
			city: {
				value: "Borne"
			},
			zip: {
				value: "7622KV"
			},
			company: {
				value: "Scribeless"
			}
		}
	};

	expect.objectContaining({
		"first name": expect("Kelvin"),
		"last name": expect("Cornelissens"),
		"address line 1": expect("Lantmanstraat 2"),
		country: expect("The Netherlands"),
		state: expect("Overijssel"),
		city: expect("Borne"),
		postal_code: expect("7622KV"),
		company: expect("Scribeless")
	});
});
