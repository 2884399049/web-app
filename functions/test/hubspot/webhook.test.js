const projectId = "hubspot-webhook-test";
process.env.GCLOUD_PROJECT = projectId;
process.env.FIRESTORE_EMULATOR_HOST = "localhost:8080";
const firebase_test = require("firebase-functions-test")();
const firebase = require("@firebase/testing");

//Mock firebase configuration
firebase_test.mockConfig({
	stripe: { key: "" },
	sendgrid: { apikey: "" },
	hubspot: { api_key: "", enabled: false },
	firestore: {
		apikey: "",
		authdomain: "",
		projectid: projectId,
		storagebucket: "123"
	},
	slack: {
		api_key: ""
	}
});

//Mock Hubspot API functions
jest.mock("../../hubspot/functions/hubspotAPI", () => ({
	exchangeForTokens: () => {},
	getAccessTokenInfo: () => {},
	getContactDataByVid: () => {},
	addTimelineEvent: () => {}
}));

//Mock Hubspot helper functions
jest.mock("../../hubspot/functions/index", () => ({
	getAccessToken(hubDoc, force = false) {
		if (!hubDoc.exists) {
			throw new Error("hubDoc does not exist");
		}
		return "accesstoken";
	},
	contactToAddress() {
		return {
			"first name": expect("Kelvin"),
			"last name": expect("Cornelissens"),
			"address line 1": expect("Lantmanstraat 2"),
			country: expect("The Netherlands"),
			state: expect("Overijssel"),
			city: expect("Borne"),
			postal_code: expect("7622KV"),
			company: expect("Scribeless")
		};
	}
}));

//Mock route functions
jest.mock("../../routes/v1/functions", () => ({
	add_recipients() {
		return {
			success: true,
			cost: 50,
			status: "Pending",
			id: 1,
			currency: "GBP"
		};
	},
	getUserCampaigns(uid) {
		return [{ id: 1 }, { id: 2 }];
	}
}));

const admin = require("firebase-admin");

admin.initializeApp({ projectId });

const db = admin.firestore();
const api_v1_functions = require("../../routes/v1/functions");
const app = require("../../hubspot/index")(api_v1_functions);
const supertest = require("supertest");

describe("Test the POST requests", () => {
	beforeEach(() => {
		initDatabase();
	});

	afterEach(() => {
		clearDatabase();
	});

	it("should return success is true", async done => {
		const query = {
			campaign_id: 1,
			"portal-id": 1256
		};

		const response = await supertest(app)
			.post("/webhook/contact?campaign_id=1")
			.type("form")
			.send(query)
			.set("Content-Type", "application/json")
			.set("Accept", "application/json");
		expect(response.statusCode).toBe(200);
		done();
	});

	it("should return Success", async done => {
		const query = {
			campaign_id: 1,
			fields: {
				"custom 1": "A string",
				"custom 2": 123,
				campaign_id: 1
			},
			origin: {
				portalId: 1256
			},
			object: {
				objectId: 12
			}
		};

		const response = await supertest(app)
			.post("/webhook/add_recipient?campaign_id=1")
			.type("form")
			.send(query)
			.set("Content-Type", "application/json")
			.set("Accept", "application/json");
		expect(response.statusCode).toBe(200);
		done();
	});

	it("should return success on campaign options", async done => {
		const query = {
			campaign_id: 1,
			portalId: 1256
		};

		const response = await supertest(app)
			.post("/webhook/campaign_options?campaign_id=1")
			.type("form")
			.send(query)
			.set("Content-Type", "application/json")
			.set("Accept", "application/json");
		expect(response.statusCode).toBe(200);
		done();
	});
});

async function initDatabase() {
	await db
		.collection("hubspot")
		.doc("1256")
		.set({
			access_token: "blahblah",
			app_id: 15,
			expires_in: 15770120302,
			refresh_token: "Blahblahblah123",
			scopes: {
				0: "contacts"
			},
			uid: "bigUIDhere123"
		});
}

function clearDatabase() {
	db.collection("hubspot")
		.doc("Test1")
		.delete();
}
