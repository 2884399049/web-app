const db = require('firebase-admin').firestore()

/**
 * Check if a campaign id is valid
 *
 * @param {String} uid User id
 * @param {String} campaign_id Id of the campaign to validate
 * @returns {Object}
 * { success: false, error_message: "", error_code: "" }
 */
exports.validateCampaignId = async (uid, campaign_id) => {
  const campaign_doc = await db.collection('campaigns').doc(campaign_id).get()
  const response = {
    success: true,
    error_message: '',
    error_code: ''
  }

  if (!campaign_doc.exists) {
    console.log(`Campaign ${campaign_id} does not exist`)

    response.success = false
    response.error_message = `Campaign ${campaign_id} does not exist`
    response.error_code = 'error/not-found'
  } else if (campaign_doc.data().uid !== uid) {
    console.log(`User ${uid} does not have access to campaign ${campaign_id}`)

    response.success = false
    response.error_message = `User ${uid} does not have access to campaign ${campaign_id}`
    response.error_code = 'error/unauthorised'
  }

  return response
}
