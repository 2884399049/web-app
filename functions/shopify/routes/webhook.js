const router = require('express').Router()
const admin = require('firebase-admin')
const HTTP_STATUS = require('http-status')
const { customerToRecipient } = require('../../lib/shopifyHelpers')
const notifications = require('../../lib/notifications')
const constants = require('../../shared/constants')

const db = admin.firestore()

const Shopify = db.collection('shopify')

let apiFunctions

/**
 * @swagger
 *  /shopify/app/uninstalled:
 *    post:
 *      tags:
 *        - Webhooks
 */
router.post('/app/uninstalled', async (req, res) => {
  const { domain: shop } = req.body
  // on delete shop or on delete app remove record from db
  const { innerShopId } = req.query
  try {
    console.log(`Shopify Webhook: app/uninstalled shop: "${shop}", innerShopId: "${innerShopId}"`)
    const shopDocRef = await Shopify.doc(shop).get()
    if (shopDocRef.exists && shopDocRef.data().innerShopId === innerShopId) {
      await Shopify.doc(shop).delete()
    } else {
      console.log(`try to delete shop "${shop}" but innerShopId does not match`)
    }
  } catch (error) {
    const webhook = 'on delete app'
    console.error(`Shopify Webhook "${webhook}": Removing Shopify record (shop: "${shop}", innerShopId: "${innerShopId}") error: `, error)
    return res.status(HTTP_STATUS.INTERNAL_SERVER_ERROR).json({success: false})
  }

  return res.status(HTTP_STATUS.OK).json({success: true})
})

router.post('/customers/create', async (req, res) => {
  const webhook = 'customers/create'
  const shop = req.shop
  try {
    const shopDocSnapshot = await Shopify.doc(shop).get()

    const { error, message } = checkShopifyDBRecord({shopDocSnapshot, webhook, shop})
    if (error) {
      console.log(message)
      // we don't need to answer with error because shopify will retry his request
      return res.status(HTTP_STATUS.OK)
    }

    const shopDoc = shopDocSnapshot.data()
    const { customerCreatedCampaignId: campaignId, uid, onCustomerCreate } = shopDoc

    console.log(onCustomerCreate)
    if (onCustomerCreate) {
      const recipient = customerToRecipient(req.body, 'default_address')

      const result = await apiFunctions.add_recipients(campaignId, [recipient], uid, undefined, constants.RECIPIENT_SOURCE.shopify)

      if (!result.success) {
        await notifications.add(uid, constants.NOTIFICATION_LEVEL.error, 'Invalid Shopify contact', `A shopify contact has insufficent details to send a letter: "${result.error_message}". Please update the contact at https://${shop}.myshopify.com/admin/customers/${req.body.id} and retrigger the letter`)
      }
    } else {
      console.log('onCreateCustomer has not been enabled')
    }
  } catch (error) {
    console.error(`Shopify Webhook "${webhook}": Sending handwriting post card error: `, error)
    return res.status(HTTP_STATUS.INTERNAL_SERVER_ERROR).json({success: false})
  }
  return res.status(HTTP_STATUS.OK).json({success: true})
})

router.post('/orders/create', async (req, res) => {
  const webhook = 'orders/create'
  const shop = req.shop
  try {
    const shopDocSnapshot = await Shopify.doc(shop).get()
    const { error, message } = checkShopifyDBRecord({shopDocSnapshot, webhook, shop})
    if (error) {
      console.log(message)
      // we don't need to answer with error because shopify will retry his request
      return res.status(HTTP_STATUS.OK)
    }

    const shopDoc = shopDocSnapshot.data()
    const { orderCreatedCampaignId: campaignId, uid, orderCreatedMinTotalPrice, onOrderCreate } = shopDoc

    const minTotalPrice = Number(orderCreatedMinTotalPrice)
    const { total_price: orderTotalPrice } = req.body

    if (minTotalPrice && orderTotalPrice && minTotalPrice > orderTotalPrice) {
      const message = `Shopify Webhook "${webhook}": Sending handwriting post card aborted because orderTotalPrice lower then minTotalPrice`
      console.log(message)
      return res.status(HTTP_STATUS.OK).json({success: true})
    }

    if (onOrderCreate) {
      const recipient = customerToRecipient(req.body, 'shipping_address')

      const result = await apiFunctions.add_recipients(campaignId, [recipient], uid, undefined, constants.RECIPIENT_SOURCE.shopify)

      if (!result.success) {
        await notifications.add(uid, constants.NOTIFICATION_LEVEL.error, 'Invalid Shopify contact', `A shopify contact has insufficent details to send a letter: "${result.error_message}". Please update the contact at https://${shop}.myshopify.com/admin/customers/${req.body.id} and retrigger the letter`)
      }
    } else {
      console.log('onOrderCreate has not been enabled by the user')
    }
  } catch (error) {
    console.error(`Shopify Webhook "${webhook}": Sending handwriting post card error: `, error)
    return res.status(HTTP_STATUS.INTERNAL_SERVER_ERROR).json({success: false})
  }
  return res.status(HTTP_STATUS.OK).json({success: true})
})

router.post('/customers/redact', async (req, res) => {
  const webhook = 'customers/redact'
  const shop = req.shop

  console.error(`REDACT ${webhook}: ${shop}`, req.body)

  return res.status(HTTP_STATUS.OK).json({success: true})
})

router.post('/shop/redact', async (req, res) => {
  const webhook = 'shop/redact'
  const shop = req.shop

  console.error(`REDACT ${webhook}: ${shop}`, req.body)

  return res.status(HTTP_STATUS.OK).json({success: true})
})

router.post('/customers/data_request', async (req, res) => {
  const webhook = 'customers/data_request'
  const shop = req.shop

  console.error(`DATA REQUEST ${webhook}: ${shop}`, req.body)

  return res.status(HTTP_STATUS.OK).json({success: true})
})

function checkShopifyDBRecord ({ shopDocSnapshot, webhook, shop }) {
  const shopDoc = shopDocSnapshot.data()
  if (!shopDocSnapshot.exists) {
    const message = `shopify: webhook "${webhook}" shop "${shop}".
       Error: it is impossible to execute "add_recipients" because shop does not exist`
    return {error: true, message}
  }

  const { uid } = shopDoc
  if (!uid) {
    const message = `shopify: webhook "${webhook}" shop "${shop}".
         Error: it is impossible to execute "add_recipients" because shop does not contain campaignId field`
    return {error: true, message}
  }

  // if (!campaignId) {
  //   const message = `shopify: webhook "${webhook}" shop "${shop}".
  //        Error: it is impossible "add_recipients" because shop is not connected to user account`;
  //   return {error: true, message};
  // }
  return {error: false}
}

module.exports = function (functions) {
  apiFunctions = functions
  return router
}
