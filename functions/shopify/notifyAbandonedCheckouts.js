const functions = require('firebase-functions')
const admin = require('firebase-admin')
const PromisePool = require('es6-promise-pool')
const { getAbandonedCheckouts } = require('./functions')
const constants = require('../shared/constants')
const { customerToRecipient } = require('../lib/shopifyHelpers')
const configs = functions.config()

const {
  api_version: apiVersion
} = configs.shopify

const db = admin.firestore()

const Shopify = db.collection('shopify')

// TODO decide
const shopifyRequestConcurrency = 5
const addRecipientsConcurrency = 5
const savingResultsConcurrency = 5

/**
 *
 * @date 2019-11-06
 * @returns {any}
 */
const notifyAbandonedCheckouts = notifyUser => async () => {
  try {
    const getShopsWithNotificationActive = async () => {
      // get shops list for which we should notify users with abandoned checkouts
      const shopsDocs = await Shopify
        .where('checkoutsAbandonedNotifyActive', '==', true)
        // .where('checkoutsAbandonedCampaignId', '==', '*')
        .select('accessToken', 'checkoutsAbandonedCampaignId', 'uid', 'createdSince')
        .get()

      if (shopsDocs.empty) {
        return []
      }

      const shops = []
      for (let shopDoc of shopsDocs.docs) {
        let shopData = shopDoc.data()
        const { accessToken, checkoutsAbandonedCampaignId: campaignId, uid, createdSince } = shopData

        if (!campaignId || !uid || !accessToken || !createdSince) {
          console.error(`notifyAbandonedCheckouts: shop "${shopDoc.id}" DB record does not have one (or more) of required fields`)
          continue
        }

        const shop = shopDoc.id
        shops.push({shop, accessToken, campaignId, uid: uid, createdSince})
      }
      return shops
    }

    const shops = await getShopsWithNotificationActive()

    const shopToAbandonedCheckoutsMap = new Map()
    {
      // get abandoned checkouts
      const generatePromises = function * (shops) {
        for (let i = 0; i < shops.length; i++) {
          const { shop, accessToken, createdSince } = shops[i]

          if (typeof createdSince !== 'object' || !createdSince.toDate) {
            console.error(`shop "${shop}" createdSince is invalid`)
            continue
          }

          const requestParams = {
            createdAtMin: (createdSince.toDate()).toISOString()
          }
          yield getAbandonedCheckouts({shop, accessToken, apiVersion, requestParams})
            .then(checkouts => {
              const createdSince = new Date()
              return shopToAbandonedCheckoutsMap.set(shop, {checkouts, createdSince})
            })
            .catch(error => {
              console.log('generatePromises error', error)
              shopToAbandonedCheckoutsMap.set(shop, {error})
            })
        }
      }

      const pool = new PromisePool(generatePromises(shops), shopifyRequestConcurrency)

      await pool.start()
    }

    const notificationsResultMap = new Map()

    const generateNotifyUserPromises = function * (shopToAbandonedCheckoutsMap) {
      for (let [ shopName, { error, checkouts } ] of shopToAbandonedCheckoutsMap.entries()) {
        const shop = shops.find(shop => shop.shop === shopName)
        if (error) {
          notificationsResultMap.set(shop, {error})
          continue
        }
        const { campaignId, uid } = shop
        notificationsResultMap.set(shopName, new Map())

        for (let i = 0; i < checkouts.length; i++) {
          const checkout = checkouts[i]
          const recipient = customerToRecipient(checkout, 'shipping_address')
          yield notifyUser(campaignId, [recipient], uid, undefined, constants.RECIPIENT_SOURCE.shopify)
            .then(result => {
              notificationsResultMap.get(shopName).set(checkout, {data: result})
            })
            .catch(error => {
              notificationsResultMap.get(shopName).set(checkout, {error})
            })
        }
      }
    }

    const notifyUserPool = new PromisePool(generateNotifyUserPromises(shopToAbandonedCheckoutsMap), addRecipientsConcurrency)

    await notifyUserPool.start()

    for (let [shopName, shopNotificationsResult] of notificationsResultMap.entries()) {
      if (typeof shopNotificationsResult === 'object' && shopNotificationsResult.error) {
        console.log(`notifyAbandonedCheckouts shop "${shopName}" error: getting shop abandoned checkouts error`, shopNotificationsResult.error)
        continue
      }
      for (let [ checkout, { error, data } ] of shopNotificationsResult.entries()) {
        if (error) {
          let message = `notifyAbandonedCheckouts: shop "${shopName}" checkoutId: "${checkout.id}" notify error: `
          console.log(message, error)
          continue
        }
        // ok
        // console.log(`notifyAbandonedCheckouts: success, shop "${shopName}" checkoutId: "${checkout.id}"`);
        // console.log('result=', data)
      }
    }

    const generateSaveNotificationPromises = function * () {
      for (let [shopName, shopNotificationsResult] of notificationsResultMap.entries()) {
        if (typeof shopNotificationsResult === 'object' && shopNotificationsResult.error) {
          const message = `notifyAbandonedCheckouts: shop "${shopName}" error: getting shop abandoned checkouts error`
          console.log(message, shopNotificationsResult.error)
          continue
        }
        const { createdSince } = shopToAbandonedCheckoutsMap.get(shopName)
        yield Shopify.doc(shopName)
          .update({createdSince})
          .catch(error => {
            error.message = `shop: ${shopName}; createdSince: ${createdSince}; error: \n` + error.message
            throw error
          })
      }
    }

    const saveNotificationResultPool = new PromisePool(generateSaveNotificationPromises(), savingResultsConcurrency)

    saveNotificationResultPool.addEventListener('rejected', (event) => {
      console.log('notifyAbandonedCheckouts: saving notification result error : ', event.data.error.message)
    })

    await saveNotificationResultPool.start()
  } catch (e) {
    console.error('notifyAbandonedCheckouts error: ', e)
  }
}

module.exports = {notifyAbandonedCheckouts}
