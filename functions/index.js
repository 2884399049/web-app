const functions = require('firebase-functions')
const admin = require('firebase-admin')
const axios = require('axios')

const configs = functions.config()
try {admin.initializeApp()} catch(err) {let a = 1}// ignored due to testing
const db = admin.firestore()

const download = require('download')

const diff = require('deep-diff').diff

const api_v1_functions = require('./routes/v1/functions')

const constants = require('./shared/constants')
const utils = require('./lib/utils')
const text_utils = require('./lib/text-utils')
const handwriting_utils = require('./lib/handwriting-utils')
const layout_utils = require('./lib/layout-utils')
const batches = require('./lib/batches')
const notifications = require('./lib/notifications')
const email_api = require('./lib/email_api')
const pricing = require('./lib/pricing')
const Hubspot = require('./lib/hubspot-utils')
const slack = require('./lib/slack')
const FieldValue = require('firebase-admin').firestore.FieldValue

const fs = require('fs')

const moment = require('moment')
const {
  notifyAbandonedCheckouts
} = require('./shopify/notifyAbandonedCheckouts')

const generateBackup = require('./backup/index')

const bucket = admin.storage().bucket(configs.firestore.storagebucket)

// https://stackoverflow.com/a/53676247
if (!fs.existsSync('/tmp/.config')){
  fs.mkdirSync('/tmp/.config');
}

async function render_recipient (recipient, campaign_id, recipient_id) {
  var campaign_ref = db.collection('campaigns').doc(campaign_id)
  var recipient_ref = campaign_ref.collection('recipients').doc(recipient_id)
  var campaign_doc = await campaign_ref.get()

  console.log(`Rendering ${recipient_id} from campaign ${campaign_id}, retry: ${recipient.retries}`)

  if (!campaign_doc.exists) {
    console.warn(`No campaign found /campaigns/${campaign_id}/recipients/${recipient_id}`)
    return
  }

  var campaign = campaign_doc.data()

  let handwriting_style = constants.STYLE_LOOKUP['Jane']
  let handwriting_engine = false

  var data = {
    status: constants.RECIPIENT_STATUS.ready_to_print
  }

  let text_size_mm = 6
  var text_size

  // TODO
  if (campaign.stationary_required) {
    var stationary_doc = await db.collection('stationary').doc(campaign.stationary).get()
    var stationary = stationary_doc.data()

    let handwriting_size = stationary.handwriting_size
      ? stationary.handwriting_size
      : 'Medium'
    switch (handwriting_size) {
      case 'Small':
        text_size_mm = 5
        break
      case 'Medium':
        text_size_mm = 6
        break
      case 'Large':
        text_size_mm = 7
        break
    }

    var text = text_utils.template_text(campaign.text, recipient, campaign.delivery.include_country_name)
    var width = parseInt(stationary.paper.width) - constants.MARGIN * 2

    handwriting_style = constants.STYLE_LOOKUP[stationary.handwriting_style]
    if (!handwriting_style) {
      handwriting_style = stationary.handwriting_style
      handwriting_engine = true
    }

    text_size = text_size_mm

    let handwriting_colour = stationary.handwriting_colour ? stationary.handwriting_colour : 'blue'

    console.log('Requesting text')
    let out = await handwriting_utils.request_handwriting(
      text,
      handwriting_style,
      text_size,
      width,
      recipient.retries,
      handwriting_colour,
      handwriting_engine
    )
    let remaining = out.remaining
    data['letter_text_url'] = out.text_url
    let text_image_path = out.text_image_path
    console.log('Layout text')

    if (!remaining) {
      await slack.post(`Recipient ${recipient_id} from campaign https://app.scribeless.co/campaign/${campaign_doc.id} is in error state`, configs.slack.channels.system_errors)
      return recipient_ref.update({
        status: constants.RECIPIENT_STATUS.error,
        retries: out.retries
      })
    }

    let alter_paper_to
    if (recipient.country === 'GB' && constants.PAPER_SIZES[stationary.paper.key].iso_standard) {
      alter_paper_to = constants.PAPER_SIZES[constants.PAPER_SIZES[stationary.paper.key].iso_standard]
    }
    if (recipient.country !== 'GB' && constants.PAPER_SIZES[stationary.paper.key].us_standard) {
      alter_paper_to = constants.PAPER_SIZES[constants.PAPER_SIZES[stationary.paper.key].us_standard]
    }

    let pdf_width = stationary.paper.width
    let pdf_height = stationary.paper.height
    if (alter_paper_to) {
      pdf_width = alter_paper_to.width
      pdf_height = alter_paper_to.height
    }

    let add_bleed = true
    if (stationary.add_bleed === false) {
      add_bleed = stationary.add_bleed
    }

    if (!stationary.back_image) {
      add_bleed = false
    }

    let front_html = layout_utils.modify_stationary_html(stationary, stationary.html, !add_bleed, alter_paper_to, data['letter_text_url'])
    let front_pdf = await layout_utils.render_html_to_pdf(front_html, pdf_width, pdf_height, add_bleed)

    let front_thumbnail_location
    let back_thumbnail_location
    if (recipient.index === 0) { // Create thumbnail for first recipient
      front_html = layout_utils.modify_stationary_html(stationary, stationary.html, true, undefined, data['letter_text_url'])
      let front_png = await layout_utils.render_html_to_png(front_html, stationary.paper.width, stationary.paper.height, false)
      let first_recipient_preview = await layout_utils.upload_image(`campaigns/${campaign_id}/thumbnail-front.png`, front_png)
      fs.unlinkSync(front_png)
      await campaign_ref.update({first_recipient_preview: first_recipient_preview})
    }

    if (stationary.back_image) {
      let back_html = layout_utils.modify_stationary_html(stationary, stationary.back_html, !add_bleed, alter_paper_to, undefined, true)
      let back_pdf = await layout_utils.render_html_to_pdf(back_html, pdf_width, pdf_height, add_bleed)
      let merged_front_pdf = await layout_utils.merge_pdf(front_pdf, back_pdf)
      fs.unlinkSync(back_pdf)
      fs.unlinkSync(front_pdf)

      front_pdf = merged_front_pdf
    }

    data['letter_url'] = await layout_utils.upload_image(`campaigns/${campaign_id}/recipients/${recipient_id}/${campaign_id}-${recipient_id}-letter.pdf`, front_pdf)
    fs.unlinkSync(front_pdf)

    // TODO upload and delete /tmp?

    // TODO first user mockup
    //   let user = await utils.get_user_profile(campaign.uid)
    //   await Hubspot.update_contact(user, user.email, {
    //     latest_campaign_mockup_url: pdf_to_png.mockup
    //   })
    //
    //   await campaign_ref.update({first_recipient_mockup: pdf_to_png.mockup})
  }

  var envelope_url = null
  var envelope_text_url = null
  if (campaign.envelope.required === constants.ENVELOPE_REQUIRED.yes) {
    console.log('Requesting envelope')

    text_size = 5 // NOTE Fixed size on envelopes

    let handwriting_colour

    if (stationary) {
      handwriting_colour = stationary.handwriting_colour ? stationary.handwriting_colour : 'blue'
    } else {
      handwriting_colour = 'blue'
    }

    let envelope_text = text_utils.template_text("{{{address}}}", recipient, campaign.delivery.include_country_name);
    let out = await handwriting_utils.request_handwriting(
      envelope_text,
      handwriting_style,
      text_size,
      120,
      recipient.retries,
      handwriting_colour,
      handwriting_engine
    )
    let remaining = out.remaining
    data['envelope_text_url'] = out.text_url
    let text_image_path = out.text_image_path

    if (!remaining) {
      await slack.post(`Recipient ${recipient_id} from campaign https://app.scribeless.co/campaign/${campaign_doc.id} is in error state`, configs.slack.channels.system_errors)
      return recipient_ref.update({
        status: constants.RECIPIENT_STATUS.error,
        retries: out.retries
      })
    }

    console.log('Layout envelope')

    var ENV_PAD_LEFT = 35
    var ENV_PAD_TOP = 25
    if (campaign.envelope.paper.width < 110) {
      ENV_PAD_LEFT = 5
    }

    if (campaign.envelope.paper.height < 110) {
      ENV_PAD_TOP = 5
    }

    let paper = campaign.envelope.paper
    if (
      (recipient.country === 'US' || recipient.country === 'CA') &&
      campaign.envelope.paper.name === 'C5'
    ) {
      paper = constants.ENVELOPE_SIZES.half_letter_envelope
    }

    data['envelope_url'] = await layout_utils.layout_letter(
      text_image_path,
      'center',
      parseFloat(paper.width),
      parseFloat(paper.height),
      ENV_PAD_LEFT,
      ENV_PAD_TOP,
      parseFloat(paper.width) - ENV_PAD_LEFT * 2,
      parseFloat(paper.height) - ENV_PAD_TOP * 2,
      `campaigns/${campaign_id}/recipients/${recipient_id}/${campaign_id}-${recipient_id}-envelope.pdf`,
      undefined
    )
  }

  console.log(data)
  return recipient_ref.update(data)
}

async function check_campaign_complete (campaign_id) {
  var recipient_docs = await db
    .collection('campaigns')
    .doc(campaign_id)
    .collection('recipients')
    .get()
  var complete = true
  var shipped = true
  recipient_docs.forEach(recipient_doc => {
    var recipient = recipient_doc.data()

    if (recipient.status === constants.RECIPIENT_STATUS.deleted) {
      return
    }

    if (
      !(
        recipient.status === constants.RECIPIENT_STATUS.ready_to_print ||
        recipient.status === constants.RECIPIENT_STATUS.shipped
      )
    ) {
      complete = false
    }

    if (recipient.status !== constants.RECIPIENT_STATUS.shipped) {
      shipped = false
    }
  })

  return {
    shipped,
    complete
  }
}

exports.setup_profile = functions.firestore.document('users/{uid}').onCreate(async (snap, context) => {
  var user_profile = snap.data()

  console.log(`UID ${context.params.uid}, setting up account`)

  if (user_profile.stripe_customer_id) {
    console.log(`Already a stripe customer, skipping...`)
  }

  let customer = await pricing.create_customer(user_profile.email)
  await db
    .collection('users')
    .doc(context.params.uid)
    .update({ stripe_customer_id: customer.id, billing_collection_method: 'charge_automatically' })
})

exports.create_check_sample_campaigns = functions.firestore
  .document('campaigns/{campaign_id}')
  .onCreate(async (snap, context) => {
    var campaign = snap.data()

    if (campaign.is_sample) {
      console.log('Removing samples')
      return db
        .collection('users')
        .doc(campaign.uid)
        .update({ samples_left: 0 })
    }
  })

exports.write_stationary = functions
  .runWith({ timeoutSeconds: 120, memory: '2GB' })
  .firestore.document('stationary/{stationery_id}')
  .onWrite(async (change, context) => {
    let stationery_doc = change.after
    let stationery = stationery_doc.data()

    let updates = {}

    if (!change.before || !change.before.data() || change.before.data().html !== change.after.data().html) {
      let front_html = layout_utils.modify_stationary_html(stationery, stationery.html, true, undefined, stationery.rendered_text)
      let front_png = await layout_utils.render_html_to_png(front_html, stationery.paper.width, stationery.paper.height, false)
      updates['thumbnail'] = await layout_utils.upload_image(`stationary/${context.params.stationery_id}/thumbnail.png`, front_png)
      fs.unlinkSync(front_png)
    }

    if ((!change.before || !change.before.data() || change.before.data().back_html !== change.after.data().back_html) && stationery.back_image) {
      let back_html = layout_utils.modify_stationary_html(stationery, stationery.back_html, true)
      let back_png = await layout_utils.render_html_to_png(back_html, stationery.paper.width, stationery.paper.height, false)
      updates['thumbnail_back'] = await layout_utils.upload_image(`stationary/${context.params.stationery_id}/thumbnail_back.png`, back_png)
      fs.unlinkSync(back_png)
    }

    if (Object.keys(updates).length > 0) {
      return stationery_doc.ref.update(updates)
    }

    return false
  })

exports.audience_updated = functions.firestore.document('audiences/{audience_id}/recipients/{recipient_id}').onWrite((change,context) => {
  var audience_id = context.params.audience_id  
  
  if (!change.before.exists) {
      //Creating new document, add one
      return db.collection('audiences').doc(audience_id).update({recipient_count: FieldValue.increment(1)})
    } 
    else if (change.before.exists && change.after.exists){
      //Updating existing document, do nothing
      return false
    }
    else if (!change.after.exists){
      // Deleting document, subtract one
      return db.collection('audiences').doc(audience_id).update({recipient_count: FieldValue.increment(-1)})
    }
})

exports.update_audience_stats = functions.pubsub.schedule('33 23 * * *').onRun(async (context) => {
  let audiences = await db.collection('audiences').get()
    for(let audience of audiences.docs){
        db.collection('stats').add({
            audience_id: audience.id,
            date: new Date(),
            recipient_count: audience.data().recipient_count,
            uid: audience.data().uid
        })
    }
})

exports.recipient_updated = functions.firestore.document('campaigns/{campaign_id}/recipients/{recipient_id}').onUpdate(async (snap, context) => {
  let campaign_id = context.params.campaign_id
  let recipient_id = context.params.recipient_id

  let recipient_before = snap.before.data()
  let recipient = snap.after.data()
  recipient['id'] = snap.after.id

  let proms = []
  if (snap.before.data() && recipient && snap.before.data().status !== recipient.status) {
    if (recipient.status === constants.RECIPIENT_STATUS.deleted || snap.before.data().status === constants.RECIPIENT_STATUS.deleted) {
      console.log(`[Recipient: ${recipient.id}] Status change from/to deleted`)
      proms.push(api_v1_functions.toggle_recipient_deleted(recipient, campaign_id))
    }
  }

  if (snap.before.data() && recipient && snap.before.data().testing !== recipient.testing) {
    console.log(`[Recipient: ${recipient.id}] Testing property changed`)
    proms.push(api_v1_functions.toggle_recipient_deleted(recipient, campaign_id))
  }

  if (snap.before.data() && recipient && snap.before.data().status !== recipient.status) {
    let recipients_count_by_status = {}
    let total_how_many = 0
    if (recipient.status === constants.RECIPIENT_STATUS.deleted) {
      total_how_many = -1
    }
    for (let status_key in constants.RECIPIENT_STATUS) {
      if (recipient.status === constants.RECIPIENT_STATUS[status_key]) {
        recipients_count_by_status[`recipients_count_${constants.RECIPIENT_STATUS[status_key]}`] = admin.firestore.FieldValue.increment(1)
      } else if (recipient_before.status === constants.RECIPIENT_STATUS[status_key]) {
        recipients_count_by_status[`recipients_count_${constants.RECIPIENT_STATUS[status_key]}`] = admin.firestore.FieldValue.increment(-1)
      } else {
        recipients_count_by_status[`recipients_count_${constants.RECIPIENT_STATUS[status_key]}`] = admin.firestore.FieldValue.increment(0)
      }
    }

    console.log(recipient.status, recipient_before.status, recipients_count_by_status)

    proms.push(db.collection('campaigns').doc(campaign_id).update({ recipients_count: admin.firestore.FieldValue.increment(total_how_many), ...recipients_count_by_status }))
  }

  return Promise.all(proms)
})

exports.new_recipient = functions
  .runWith({ timeoutSeconds: 120, memory: '2GB' })
  .firestore.document('campaigns/{campaign_id}/recipients/{recipient_id}')
  .onCreate(async (snap, context) => {
    var campaign_id = context.params.campaign_id
    var recipient_id = context.params.recipient_id

    var recipient = snap.data()

    let recipients_count_by_status = {}
    for (let status_key in constants.RECIPIENT_STATUS) {
      if (recipient.status === constants.RECIPIENT_STATUS[status_key]) {
        recipients_count_by_status[`recipients_count_${constants.RECIPIENT_STATUS[status_key]}`] = admin.firestore.FieldValue.increment(1)
      } else {
        recipients_count_by_status[`recipients_count_${constants.RECIPIENT_STATUS[status_key]}`] = admin.firestore.FieldValue.increment(0)
      }
    }

    return db.collection('campaigns').doc(campaign_id).update({ recipients_count: admin.firestore.FieldValue.increment(1), ...recipients_count_by_status })
  })

exports.rerender_recipient = functions
  .runWith({ timeoutSeconds: 120, memory: '2GB' })
  .https.onCall(async (data, context) => {
    let campaign_id = data.campaign_id
    let recipient_id = data.recipient_id

    let recipient_doc = await db.collection('campaigns').doc(campaign_id).collection('recipients').doc(recipient_id).get()
    let recipient = recipient_doc.data()
    recipient.status = constants.RECIPIENT_STATUS.pending

    return render_recipient_function(campaign_id, recipient_id, recipient)
})

async function render_recipient_function(campaign_id, recipient_id, recipient) {
  let proms = []
  if (recipient && recipient.status === constants.RECIPIENT_STATUS.pending) {
    proms.push(db.collection('campaigns')
      .doc(campaign_id)
      .update({ status: constants.CAMPAIGN_STATUS.in_progress, zip: null }))

    proms.push(db.collection('campaigns')
      .doc(campaign_id)
      .collection('recipients')
      .doc(recipient_id)
      .update({
        status: constants.RECIPIENT_STATUS.in_progress,
        envelope_url: null,
        letter_url: null,
        start_rendering_at: new Date()
      }))

    proms.push(render_recipient(recipient, campaign_id, recipient_id))
  }

  return Promise.all(proms)
}

exports.render_recipient = functions
  .runWith({ timeoutSeconds: 120, memory: '2GB' })
  .firestore.document('campaigns/{campaign_id}/recipients/{recipient_id}')
  .onWrite(async (snap, context) => {
    var campaign_id = context.params.campaign_id
    var recipient_id = context.params.recipient_id

    var recipient = snap.after.data()

    if (recipient.testing) { // change status to shipped
      await render_recipient_function(campaign_id, recipient_id, recipient)
      return snap.after.ref.update({status: constants.RECIPIENT_STATUS.shipped})
    } else {
      return render_recipient_function(campaign_id, recipient_id, recipient)
    }
})

exports.interface_render_text = functions
  .runWith({ timeoutSeconds: 120, memory: '2GB' })
  .https.onCall(async (data, context) => {

    let variables = data.variables || undefined
    let include_country_name = data.include_country_name || false
    var text = text_utils.template_text(data.text, variables, include_country_name)
    var result = text_utils.check_bad_text(text)
    text = result[1]

    // BOTE don't validate bad text
    // if (!result[0]) {
    //   console.warn(result)
    //   return {success: false, error_message: result[2], error_code: "error/invalid-text"}
    // }

    var width = data.paper.width
    var text_size = data.text_size
    var handwriting_style = constants.STYLE_LOOKUP[data.handwriting_style]
    let handwriting_colour = data.handwriting_colour
    let handwriting_engine = false

    if (!handwriting_style) {
      handwriting_style = data.handwriting_style
      handwriting_engine = true
    }

    return handwriting_utils.request_handwriting(
      text,
      handwriting_style,
      text_size,
      width,
      undefined,
      handwriting_colour,
      handwriting_engine
    )
  })

exports.add_recipients = functions
  .runWith({ timeoutSeconds: 120, memory: '1GB' })
  .https.onCall(async (data, context) => {
    if (data.test) {
      return null
    }

    var testing = false
    if (data.testing) {
      testing = true
      console.log(`API Testing mode`)
    }

    return api_v1_functions.add_recipients(
      data.campaign_id,
      data.recipients,
      context.auth.uid,
      testing,
      data.source
    )
  })

exports.campaign_updated = functions
  .runWith({ timeoutSeconds: 540, memory: '2GB' })
  .firestore.document('campaigns/{campaign_id}')
  .onUpdate(async (change, context) => {
    let before_campaign = change.before.data()
    var campaign = change.after.data()
    campaign['id'] = change.after.id

    // Status changed
    if (change.before.data() && campaign && change.before.data().status !== campaign.status) {
      if (campaign.status === constants.CAMPAIGN_STATUS.deleted) {
        return api_v1_functions.mark_campaign_as_deleted(campaign, campaign.updated_by)
      }

      if (campaign.status === constants.CAMPAIGN_STATUS.error) {
        await slack.post(`⚠️ Campaign marked as error. https://app.scribeless.co/campaign/${context.params.campaign_id}`, configs.slack.channels.operations)
      }
    }

    if (before_campaign.print_override_location !== campaign.print_override_location) {
      await slack.post(`⚠️ Print override changed on campaign to ${campaign.print_override_location}. https://app.scribeless.co/campaign/${context.params.campaign_id}`, configs.slack.channels.operations)
    }

    if (campaign.zip !== false) {
      return false
    }

    console.log(`Zipping campaign ${change.after.id}`)

    var recipient_docs = await db
      .collection('campaigns')
      .doc(change.after.id)
      .collection('recipients')
      .get()

    var urls = {}
    recipient_docs.forEach(recipient_doc => {
      var recipient = recipient_doc.data()

      if (recipient.status !== constants.RECIPIENT_STATUS.ready_to_print) {
        return
      }

      if (recipient.letter_url) {
        urls[recipient_doc.id + '-letter'] = {url: recipient.letter_url, folder: 'pdf'}
      }

      if (recipient.envelope_url) {
        urls[recipient_doc.id + '-envelope'] = {url: recipient.envelope_url, folder: 'pdf'}
      }
    })

    let zip_url
    try {
      zip_url = await batches.zip_list_of_files(change.after.id, urls)
    } catch (err) {
      console.error('Zip error', err)

    }


    if (!zip_url) {
      console.warn(`Failed to ZIP campaign ${change.after.id}`)
      return db.collection('campaigns')
        .doc(change.after.id)
        .update({ zip: null })
    }

    return db
      .collection('campaigns')
      .doc(change.after.id)
      .update({ zip: zip_url })
  })

exports.zip_batches = functions
  .runWith({ timeoutSeconds: 540, memory: '2GB' })
  .firestore.document('batches/{batch_id}')
  .onUpdate(batches.zip_batches_function)

exports.batch_printer_check = functions.runWith({ timeoutSeconds: 540, memory: '2GB' }).https.onCall(batches.batch_printer_check_function)

async function retry_failed_renders () {
  let max_retrying = 0
  var promises = []
  let campaign_docs = await db
    .collection('campaigns')
    .where('status', '==', constants.CAMPAIGN_STATUS.in_progress)
    .get()
  for (var campaign_doc of campaign_docs.docs) {
    let recipient_docs = await db
      .collection('campaigns')
      .doc(campaign_doc.id)
      .collection('recipients')
      .where('status', '==', constants.RECIPIENT_STATUS.error)
      .where('retries', '>', 0)
      .get()

    let docs = recipient_docs.docs

    // start_rendering_at
    let start = moment()
      .subtract(121, 'seconds')
      .toDate()
    recipient_docs = await db
      .collection('campaigns')
      .doc(campaign_doc.id)
      .collection('recipients')
      .where('status', '==', constants.RECIPIENT_STATUS.in_progress)
      .where('start_rendering_at', '<', start)
      .get()

    docs = docs.concat(recipient_docs.docs)

    for (var recipient_doc of docs) {
      if (max_retrying > 1000) {
        // Only do 1K at a time
        break
      }
      max_retrying++
      console.log(
        `Queueing campaign ${campaign_doc.id} recipient ${recipient_doc.id} for rerendering`
      )
      promises.push(
        recipient_doc.ref.update({ status: constants.RECIPIENT_STATUS.pending })
      )
    }
  }

  return Promise.all(promises)
}

async function mark_campaigns_as_complete () {
  let campaign_pending_docs = await db
    .collection('campaigns')
    .where('status', '==', constants.CAMPAIGN_STATUS.pending)
    .get()
  let campaign_docs = await db
    .collection('campaigns')
    .where('status', '==', constants.CAMPAIGN_STATUS.in_progress)
    .get()
  let campaign_ready_to_ship_docs = await db
    .collection('campaigns')
    .where('status', '==', constants.CAMPAIGN_STATUS.ready_to_ship)
    .get()

  let campaigns = campaign_docs.docs.concat(campaign_ready_to_ship_docs.docs.concat(campaign_pending_docs.docs))

  var promises = []
  for (var campaign_doc of campaigns) {
    let campaign = campaign_doc.data()
    let result = await check_campaign_complete(campaign_doc.id)
    if (!result.complete) {
      console.log(`${campaign_doc.id} campaign not complete`)
      continue
    }

    let data = {}
    data['status'] = constants.CAMPAIGN_STATUS.ready_to_ship
    if (campaign.product === constants.PRODUCT.self_service || result.shipped) {
      data['status'] = constants.CAMPAIGN_STATUS.complete
    }

    if (data['status'] === campaign.status) { // No change
      continue
    }

    if (campaign.product === constants.PRODUCT.self_service) {
      data['zip'] = false
    }

    promises.push(
      db
        .collection('campaigns')
        .doc(campaign_doc.id)
        .update(data)
    )
  }
  return Promise.all(promises)
}

exports.retry_failed_renders = functions
  .runWith({ timeoutSeconds: 120, memory: '2GB' })
  .https.onCall(retry_failed_renders)
exports.retry_failed_renders_cron = functions
  .runWith({ timeoutSeconds: 120, memory: '2GB' })
  .pubsub.schedule('*/15 * * * *')
  .timeZone('Europe/London')
  .onRun(retry_failed_renders)
exports.mark_campaigns_as_complete = functions.pubsub
  .schedule('*/15 * * * *')
  .timeZone('Europe/London')
  .onRun(mark_campaigns_as_complete)

exports.email_api = functions.https.onRequest(email_api.parse_email)

// exports.email_api_fetch_emals = functions.https.onCall(email_api.fetch_emals);
// exports.email_api_fetch_emals_sched = functions.pubsub
//   .schedule("*/15 * * * *")
//   .timeZone("Europe/London")
//   .onRun(email_api.fetch_emals);

exports.send_to_batches = functions.runWith({ timeoutSeconds: 120, memory: '2GB' }).https.onCall(batches.send_to_batches)
exports.batch_cron = functions.runWith({ timeoutSeconds: 120, memory: '2GB' }).pubsub
  .schedule('0 2 * * *')
  .timeZone('Europe/London')
  .onRun(batches.send_to_batches)

exports.api = functions
  .runWith({ timeoutSeconds: 120, memory: '2GB' })
  .https.onRequest((...args) => {
    let apiRoutes = require('./routes')
    return apiRoutes(...args)
  })

exports.shopify = functions.https.onRequest((...args) => {
  let shopify = require('./shopify')(api_v1_functions)
  return shopify(...args)
})
exports.notify_abandoned_checkouts_shopify_customers_cron = functions.pubsub
  .schedule('0 3 * * *')
  .timeZone('Europe/London')
  .onRun(notifyAbandonedCheckouts(api_v1_functions.add_recipients))

exports.hubspot = functions.https.onRequest((...args) => {
  let hubspot = require('./hubspot')(api_v1_functions)
  return hubspot(...args)
})

/**
 * Run shopify and api webhooks to avoid cold boot
 */
exports.scheduledFunction = functions.pubsub
  .schedule('every 1 minutes')
  .onRun(context => {
    console.log('This will be run every 1 minutes!')

    const SERVICE_ADDRESS = configs.firestore.functions_url

    const url = {
      stripe_add_card: SERVICE_ADDRESS + '/stripe_add_card',
      add_recipients: SERVICE_ADDRESS + '/add_recipients',
      stripe_request_setupintent:
        SERVICE_ADDRESS + '/stripe_request_setupintent/',
      stripe_webhook: SERVICE_ADDRESS + '/stripe_webhook/',
      main_api: SERVICE_ADDRESS + '/api/v1/pong/'
    }

    Object.keys(url).forEach(key => {
      ping(url[key])
    })

    const data = {}

    return null
  })

/**
 * Run backup 00:00 server time each day
 */
exports.automatedBackups = functions.pubsub
  .schedule('0 0 * * *')
  .onRun(generateBackup)

/**
 * Ping the shopify webhook to avoid cold start
 *
 * @param {*} url
 */
async function ping (url) {
  const headers = {
    'Content-Type': 'application/json'
  }

  const data = {
    data: {
      test: true
    }
  }

  axios
    .post(url, data, {
      headers: headers
    })
    .then(response => {
      // console.log(response)
      return response
    })
    .catch(error => {
      return error
    })
}

exports.notify = functions.https.onCall(notifications.notify)
exports.notify_cron = functions.pubsub.schedule('*/30 * * * *').timeZone('Europe/London').onRun(notifications.notify)

async function auditOnWrite (change, context) {
  let event_type = 'Unknown'
  if (context.eventType === 'providers/cloud.firestore/eventTypes/document.write') {
    event_type = 'write'
  } else if (context.eventType === 'providers/cloud.firestore/eventTypes/document.create') {
    event_type = 'create'
  } else if (context.eventType === 'providers/cloud.firestore/eventTypes/document.update') {
    event_type = 'update'
  } else if (context.eventType === 'providers/cloud.firestore/eventTypes/document.delete') {
    event_type = 'delete'
  }

  let audit_data = {
    'auth_type': context.authType || 'unknown',
    'timestamp': context.timestamp,
    'edit_type': event_type
  }

  let before = {}
  if (change.before.exists && change.before.data()) {
    before = change.before.data()

    // Remove time fields
    if (before['created_at']) {
      delete before['created_at']
    }
    if (before['updated_at']) {
      delete before['updated_at']
    }
    if (before['updated_by']) {
      delete before['updated_by']
    }
    if (before['updated']) {
      delete before['updated']
    }

    for (let status_key in constants.RECIPIENT_STATUS) {
      if (before[`recipients_count_${constants.RECIPIENT_STATUS[status_key]}`]) {
        delete before[`recipients_count_${constants.RECIPIENT_STATUS[status_key]}`]
      }
    }
  }

  let after = {}
  if (change.after.exists && change.after.data()) {
    after = change.after.data()

    if (after.updated_by) {
      audit_data['uid'] = after.updated_by

      await change.after.ref.update({updated_by: null})

      try {
        let user_doc = await db.collection('users').doc(after.updated_by).get()
        audit_data['email'] = user_doc.data().email
      } catch (err) {
        console.log(err)
      }
    }

    // Remove time fields
    if (after['created_at']) {
      delete after['created_at']
    }
    if (after['updated_at']) {
      delete after['updated_at']
    }
    if (after['updated_by']) {
      delete after['updated_by']
    }
    if (after['updated']) {
      delete after['updated']
    }
    for (let status_key in constants.RECIPIENT_STATUS) {
      if (after[`recipients_count_${constants.RECIPIENT_STATUS[status_key]}`]) {
        delete after[`recipients_count_${constants.RECIPIENT_STATUS[status_key]}`]
      }
    }
  }

  let object_diff = diff(before, after)

  let diffs = []
  for (let diff of object_diff || []) {
    if (diff.path[0] !== 'updated_by') {
      diffs.push(JSON.parse(JSON.stringify(diff)))
    }
  }

  if (diffs.length === 0) { // Objects are the same
    return false
  }

  audit_data['diff'] = diffs

  if (context.auth && context.auth.uid) {
    audit_data['uid'] = context.auth.uid
  }

  return change.after.ref.collection('audit').doc().set(audit_data)
}

let referencesToAudit = ['users/{id}', 'printers/{id}', 'invoices/{id}', 'stationary/{id}', 'shopify/{id}', 'hubspot/{id}', 'campaigns/{id}', 'campaigns/{campaignId}/recipients/{id}', 'batches/{id}', 'printers/{id}']
for (let reference of referencesToAudit) {
  let name = `audit_${reference.replace(/\//g, '_').replace(/{/g, '').replace(/}/g, '')}`
  exports[name] = functions.firestore.document(reference).onWrite(auditOnWrite)
}

/************
 * Billing
 ***********/

exports.stripe_add_coupon = functions.https.onCall(pricing.stripe_add_coupon)
exports.stripe_request_setupintent = functions.https.onCall(pricing.stripe_request_setupintent)
exports.stripe_webhook = functions.https.onRequest(pricing.stripe_webhook)
exports.stripe_add_card = functions.https.onCall(pricing.stripe_add_card)

exports.update_aggregate_usage = functions.firestore.document('/usage/{usage_id}').onCreate(async (snapshot, context) => {
  const _usage = snapshot.data()
  let user = await utils.get_user_profile(_usage.uid)

  // Don't aggregate if invoice_doc already set
  if (_usage.dont_trigger_aggregation) {
    return
  }

  let {goods, usage, recipients_count} = await pricing.aggregate_usage_records(user)

  if (!user.billing_period_start_date) {
    console.log(`[${user.id}] No existing billing_period, starting new billing period`)
    await pricing.start_new_billing_period(user)
  }

  let invoice = await pricing.find_draft_invoice(user)

  if (!invoice) {
    console.error(`No draft invoice found`)
    return
  }

  let price_table = await pricing.build_price_table(user)
  let price = pricing.caluclate_price(price_table, user, goods)

  await db.collection('users').doc(user.id).update({billing_period_recipients_count: recipients_count})

  invoice.total = price.total
  invoice.total_with_tax = price.total_with_tax
  invoice.total_gbp = price.total_gbp
  invoice.recipients_count = recipients_count
  invoice.goods = goods
  invoice.goods_with_price = price.goods_with_price

  await Hubspot.update_deal_for_invoice(user, invoice)

  return db.collection('invoices').doc(invoice.id).update({
    total: invoice.total,
    total_with_tax: invoice.total_with_tax,
    total_gbp: invoice.total_gbp,
    recipients_count: invoice.recipients_count,
    goods: invoice.goods,
    goods_with_price: invoice.goods_with_price
  })
})

exports.bill_user = functions.https.onCall(pricing.end_of_billing_period_function)
exports.bill_users_sched = functions.pubsub.schedule('30 23 * * *').timeZone('Europe/London').onRun(pricing.check_billing_period)
exports.sync_hubspot_invoices = functions.https.onCall(pricing.sync_hubspot_invoices)
exports.bill_users = functions.https.onCall(pricing.check_billing_period)


async function add_referral(snap, context) {
  let user = snap.data()
  let referrer = user.referrer;
  try {
    if(referrer) {
      let referral_docs = await db.collection('referrals')
        .where('referrer', '==', referrer)
        .get();
      
      let is_referral_empty = referral_docs.empty;
      let referral_length = 0;
      if(!is_referral_empty) {
        referral_length = referral_docs.docs.length;
      }

      if(is_referral_empty || referral_length < 10 ) {
        /* Add referral */
        await db.collection('referrals').add({
          referrer,
          user: snap.id,
          created: new Date(),
          last_updated: new Date()
        });
        console.log(`Added referral document for referrer ${referrer}`);

        /* Update the referral stats */
        const referral_stats_ref = db.collection('referral_stats').doc(referrer);
        const statDoc = await referral_stats_ref.get()

        if (statDoc.exists) {
          const increment = FieldValue.increment(1)
          referral_stats_ref.update({
            total_referred_users: increment
          })
          .then(() => {
            console.log(`Updated referral stats for the referrer ${referrer}`);
          });
        } else {
          referral_stats_ref.set({
            total_referred_users: 1
          })
          .then(() => {
            console.log(`Added referral stats for the referrer ${referrer}`);
          });
        }

        /* update the referrer credit */
        db.collection('users').doc(referrer)
          .update({ credit: FieldValue.increment(constants.REFERRAL_CREDIT) })
          .then(() => {
            console.log(`Credit updated for referrer: ${referrer}`);
          });

      } else {
        console.log(`Referral exceeded for referrer ${referrer}`)
      }

      //delete the referrer field from the created user.
      db.collection('users').doc(snap.id)
        .update({ referrer: FieldValue.delete() })
        .then(() => {
          console.log('Removed the referrer field from the newly created user');
        })
    }

  } catch(err) {
    console.log('Error when adding/updating user credit', err)
  }

}
exports.add_referral = functions.firestore
  .document('users/{user_id}')
  .onCreate(add_referral)