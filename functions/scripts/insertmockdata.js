const { log, error: logError } = console

const firebase = require('firebase')
const functions = require('firebase-functions')

const mockData = require('../mockdata')

const { firestore, environment } = functions.config()

firebase.initializeApp({
  apiKey: firestore.apikey,
  authDomain: firestore.authdomain,
  projectId: firestore.projectid
})

const db = firebase.firestore()

const deleteCollections = async (collectionNames) => {
  // if null, undefined, 0 or empty array
  if (!collectionNames || !collectionNames.length) {
    throw new Error('Function requires an argument as an array')
  }

  for (const collectionName of collectionNames) {
    const snapshot = await db.collection(collectionName).get()
    // When there are no documents then nothing to delete
    if (snapshot.size === 0) {
      log('Nothing to delete')
      break
    }

    log(`There are ${snapshot.size} documents to delete`)
    const batch = db.batch()
    snapshot.docs.forEach((doc) => {
      batch.delete(doc.ref)
    })

    await batch.commit()
  }
}

if (!environment) {
  logError('\nYou must have the right config in your .runtimeconfig.json. \nRun the following command\n\nfirebase use test && firebase functions:config:get > .runtimeconfig.json\n')
  return
}

if (environment.name === 'testing') {
  log('\nMockdataInsert initiated\n')
  const collectionNames = Object.keys(mockData)

  // Lets delete the whole database
  deleteCollections(collectionNames)
    .then(() => {
      log('Delete was completed')

      const [pricesCollectionName, paymentCodesCollectionName] = collectionNames

      // Let's insert prices
      log(`\n\nLet's insert prices`)
      const pricesDocuments = mockData[pricesCollectionName]
      pricesDocuments.forEach(pricesDocument => {
        const docId = pricesDocument.document
        const fields = pricesDocument.fields

        log(`\nReady to insert into document: ${docId}`)
        db.collection(pricesCollectionName)
          .doc(docId)
          .set(fields)
          .then(() => log('Prices Document written with id', docId))
          .catch(error => logError('Error adding document into prices: ', error))
      })

      // Let's insert payment_codes
      log(`\n\nLet's insert payment_codes`)
      const paymentCodesDocuments = mockData[paymentCodesCollectionName]
      paymentCodesDocuments.forEach(paymentCodesDocument => {
        const docId = paymentCodesDocument.document
        const fields = paymentCodesDocument.fields

        log(`\nReady to insert into document: ${docId}`)

        let query = null
        if (!docId) {
          query = db.collection(paymentCodesCollectionName)
            .add(fields)
        } else {
          query = db.collection(paymentCodesCollectionName)
            .doc(docId)
            .set(fields)
        }

        query
          .then(docRef => log('PaymentCodes Document written with ID: ', docRef.id))
          .catch(error => logError('Error adding document into payment_codes: ', error))
      })
    })
    .catch(error => logError('Error while deleting', error))
} else {
  log('This must only run in testing environment')
}
