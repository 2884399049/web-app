FROM openjdk:13-alpine
RUN apk add --update nodejs npm
RUN apk add --update python
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
COPY . .
EXPOSE 8080
CMD [ "/bin/sh" ]