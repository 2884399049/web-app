const limit = 50

async function break_up_query(original_query, action, start_after) {
  let query = original_query.limit(limit)
  if (start_after) {
    query = query.startAfter(start_after)
  }

  let docs = await query.get()

  let proms = []
  let last_snapshot
  for (let snapshot of docs.docs) {
    let data = snapshot.data()
    data.id = snapshot.id
    proms.push(action(data, snapshot.ref))
    last_snapshot = snapshot
  }

  await Promise.all(proms)

  if (last_snapshot) {
    await break_up_query(original_query, action, last_snapshot)
  }
}

async function delete_collection(ref) {
  await break_up_query(ref, (data, ref) => {return ref.delete()})
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

module.exports.break_up_query = break_up_query
module.exports.sleep = sleep
module.exports.delete_collection = delete_collection
