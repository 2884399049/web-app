const readline = require('readline-sync')
const admin = require('firebase-admin')

admin.initializeApp({
  credential: admin.credential.cert(require("../config/hc-application-interface-test.json")),
  databaseURL: 'https://hc-application-interface-test.firebaseio.com'
})

async function main() {
  let answer = readline.question('This will delete the TEST users! Are you sure you want to continue [yN]? ')
  if (answer !== 'y') {
    console.log('Exiting.')
    return
  }

  console.log('Deleting users...')
  await delete_users()
  console.log('Deleted.')
}

main()

// firebase use dev; firebase firestore:delete --all-collections --project dev

async function delete_users(nextPageToken) {
  let listUsersResult = await admin.auth().listUsers(100, nextPageToken)
  let proms = []
  listUsersResult.users.forEach(function(userRecord) {
    uid = userRecord.toJSON().uid
    proms.push(admin.auth().deleteUser(uid))
    console.log(`Deleting ${uid}`)
  })

  await Promise.all(proms)
  if (listUsersResult.pageToken) {
    await delete_users(listUsersResult.pageToken);
  }
}
