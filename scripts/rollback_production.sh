#!/bin/bash

# Ensure has access to bucket
# gsutil iam ch serviceAccount:[SOURCE_PROJECT_ID]@appspot.gserviceaccount.com:admin gs://[BUCKET_NAME]

BUCKET_NAME="scribeless-firestore-backup-prod"
PROJECT="hc-application-interface-prod"

FILE_NAME="" # TODO
FILE="gs://$BUCKET_NAME/syncs/$FILE_NAME"

echo "Restoring production to $FILE"

gcloud config set project "$PROJECT"
gcloud firestore import $FILE
