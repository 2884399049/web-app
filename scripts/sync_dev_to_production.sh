#!/bin/bash

# Ensure has access to bucket
# gsutil iam ch serviceAccount:[SOURCE_PROJECT_ID]@appspot.gserviceaccount.com:admin gs://[BUCKET_NAME]

BUCKET_NAME="scribeless-firestore-backup-dev"
FROM_PROJECT="hc-application-interface-prod"
TO_PROJECT="hc-application-interface-dev"

# Find on firebase auth settings (These are from PROD)
HASH_KEY="+vnoWqulKSZro3ExQdAj3m6/JEDjM5asOhjKNlxpLTy921A5JBiq2mD9Mhn8sQ9IZkvqclnhpIgGH7a2zH0ilQ=="
SALT_SEPARATOR="Bw=="
ROUNDS=8
MEM_COST=14


gcloud config set project "$FROM_PROJECT"
gcloud firestore export "gs://$BUCKET_NAME/syncs/$FROM_PROJECT-export"

gcloud config set project "$TO_PROJECT"
gcloud firestore import "gs://$BUCKET_NAME/syncs/$FROM_PROJECT-export"

# --- Copy users
firebase auth:export auth.export.json --project "$FROM_PROJECT"
firebase auth:import auth.export.json --project "$TO_PROJECT" --hash-algo=SCRYPT --hash-key=$HASH_KEY --salt-separator="$SALT_SEPARATOR" --rounds="$ROUNDS" --mem-cost="$MEM_COST"
rm auth.export.json
