#!/bin/bash

# Ensure has access to bucket
# gsutil iam ch serviceAccount:[SOURCE_PROJECT_ID]@appspot.gserviceaccount.com:admin gs://[BUCKET_NAME]

BUCKET_NAME="scribeless-firestore-backup"
PROJECT="hc-application-interface-prod"

FILE_NAME="backup-$PROJECT-$(date +"%Y-%m-%d_%H-%M-%S")"
FILE="gs://$BUCKET_NAME/manual_backups/$FILE_NAME"

echo "Creating backup to $FILE"

gcloud config set project "$PROJECT"
gcloud firestore export $FILE
