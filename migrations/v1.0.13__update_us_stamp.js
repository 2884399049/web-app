let constants = require('../functions/shared/constants')
const utils = require('../scripts/utils')

let products = {}

products[constants.GOOD.stamp_us_international] = {
  goods: [constants.GOOD.stamp_us_international],
  tiers: {
    'eur': {1: 120 * constants.CURRENCY.usd_to_eur},
    'usd': {1: 120},
    'gbp': {1: 120 * constants.CURRENCY.usd_to_gbp}
  }
}


module.exports.migrate = async ({firestore}) => {
  let proms = []
  for (let product_key in products) {
    let product = products[product_key]
    proms.push(firestore.collection('products').doc(product_key).set(product))
  }

  return Promise.all(proms)
}
