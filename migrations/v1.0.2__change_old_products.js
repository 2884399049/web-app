module.exports.migrate = async ({firestore}) => {
  let campaign_docs = await firestore.collection('campaigns').get()
  let proms = []
  for (let campaign_doc of campaign_docs.docs) {
  	let campaign = campaign_doc.data()
    campaign.id = campaign_doc.id

  	let new_product
    if (campaign.product === 'On Demand') {
  		new_product = 'Self Service'
  	} else if (campaign.product === 'Advanced') {
  		new_product = 'Full Service'
  	} else if (campaign.product === 'Robotics') {
  		new_product = 'Full Service'
  	}

    if (new_product) {
      proms.push(campaign_doc.ref.update({
  			product: new_product
  		}))
    }
  }

  return Promise.all(proms)
}
