const utils = require('../scripts/utils')
let constants = require('../functions/shared/constants')

async function action(user, ref) {
  let products_enabled = [constants.PRODUCT.full_service]
  if (user.products_enabled) {
    for (let product of user.products_enabled) {
      if (product === 'On Demand') {
        products_enabled.push(constants.PRODUCT.self_service)
      }
      if (product === 'Advanced Envelope Only') {
        products_enabled.push(constants.PRODUCT.full_service_envelope_only)
      }
      if (product === 'Advanced Note Only') {
        products_enabled.push(constants.PRODUCT.full_service_note_only)
      }
      if (product === 'Robotics') {
        console.warn(`User ${user.email} (${user.id}) has robotics product enabled but this is not currently offered`)
      }
    }

    return ref.update({products_enabled: products_enabled})
  }
}

module.exports.migrate = async ({firestore}) => {
  await utils.break_up_query(firestore.collection('users'), action)
}
