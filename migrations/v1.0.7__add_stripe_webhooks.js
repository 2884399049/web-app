let constants = require('../functions/shared/constants')

module.exports.migrate = async ({firestore}) => {
  console.log(`Add stripe webhook pointing to https://us-central1-hc-application-interface-ENV.cloudfunctions.net/stripe_webhook for [customer.updated, invoice.updated, payment_method.updated, payment_method.attached, payment_method.detached]`)
}
