let constants = require('../functions/shared/constants')
const utils = require('../scripts/utils')

const lead_time_code = `// Returns an array of time in days from today if order was shipped for it to arrive per recipient
function main(campaign, recipients) {
  let out = []

  if (campaign.delivery.sender === 'Ship for me' || campaign.delivery.sender === 'Ship to self') {
    // No change when shipping to self
  }

  let lead_time = 3

  let day = new Date().getDay()
  if (day === 0) {
    lead_time += 1
  } else if (day === 6) {
    lead_time += 2
  }

  for (let recipient of recipients) {
    let days = lead_time
    out.push(days)
  }

  return out
}`

const lead_time_code_us = `// Returns an array of time in days from today if order was shipped for it to arrive per recipient
function main(campaign, recipients) {
  let out = []

  if (campaign.delivery.sender === 'Ship for me' || campaign.delivery.sender === 'Ship to self') {
    // No change when shipping to self
  }

  let lead_time = 4

  let day = new Date().getDay()
  if (day === 0) {
    lead_time += 1
  } else if (day === 6) {
    lead_time += 2
  }

  for (let recipient of recipients) {
    let days = lead_time
    out.push(days)
  }

  return out
}`

let profit_code_scribeless = `// Returns the cost in pence given goods
function main(goods) {
  let total = 0
  for (let good in goods) {
    let quantity = goods[good]

    if (good === constants.GOOD.full_service_a4_pack || good === constants.GOOD.full_service_a5_pack || good === constants.GOOD.full_service_a6_pack) {
      total += 200 * quantity
    } else if (good === constants.GOOD.full_service_a4 || good === constants.GOOD.full_service_a5 || good === constants.GOOD.full_service_a6) {
      total += 100 * quantity
    } else if (good === constants.GOOD.full_service_c5 || good === constants.GOOD.full_service_c6 || good === constants.GOOD.full_service_dl) {
      total += 100 * quantity
    } else if (good === constants.GOOD.stamp_us_first_class) {
      total += 55 * quantity
    } else if (good === constants.GOOD.insert_light) {
      total += 50 * quantity
    } else if (good === constants.GOOD.stamp_gb_first_class) {
      total += 70 * quantity
    } else if (good === constants.GOOD.stamp_gb_second_class) {
      total += 62 * quantity
    } else if (good === constants.GOOD.stamp_gb_europe) {
      total += 135 * quantity
    } else if (good === constants.GOOD.stamp_gb_world_zone_1 || good === constants.GOOD.stamp_gb_world_zone_2) {
      total += 155 * quantity
    } else if (good === constants.GOOD.ship_to_self_gb) {
      total += 3000 * quantity
    } else {
      alert(\`Unknown good: \${good}\`)
    }
  }

  let tax = 0 * total // Don't charage extra VAT
  total += tax

  return Math.ceil(total) // Round up
}`

let profit_code_printed_word = `// Returns the cost in pence given goods
function main(goods) {
  let total = 0
  for (let good in goods) {
    let quantity = goods[good]

    if (good === constants.GOOD.full_service_a4_pack || good === constants.GOOD.full_service_a5_pack || good === constants.GOOD.full_service_a6_pack) {
      total += 200 * quantity
    } else if (good === constants.GOOD.full_service_a4 || good === constants.GOOD.full_service_a5 || good === constants.GOOD.full_service_a6) {
      total += 100 * quantity
    } else if (good === constants.GOOD.full_service_c5 || good === constants.GOOD.full_service_c6 || good === constants.GOOD.full_service_dl) {
      total += 100 * quantity
    } else if (good === constants.GOOD.stamp_us_first_class) {
      total += 55 * quantity
    } else if (good === constants.GOOD.insert_light) {
      total += 50 * quantity
    } else if (good === constants.GOOD.stamp_gb_first_class) {
      total += 70 * quantity
    } else if (good === constants.GOOD.stamp_gb_second_class) {
      total += 62 * quantity
    } else if (good === constants.GOOD.stamp_gb_europe) {
      total += 135 * quantity
    } else if (good === constants.GOOD.stamp_gb_world_zone_1 || good === constants.GOOD.stamp_gb_world_zone_2) {
      total += 155 * quantity
    } else if (good === constants.GOOD.ship_to_self_gb) {
      total += 3000 * quantity
    } else {
      alert(\`Unknown good: \${good}\`)
    }
  }

  let tax = 0.20 * total
  total += tax

  return Math.ceil(total) // Round up
}`

let profit_code_caddy = `// Returns the cost in pence given goods
function main(goods) {
  let total = 0
  for (let good in goods) {
    let quantity = goods[good]

    if (good === constants.GOOD.full_service_a4_pack || good === constants.GOOD.full_service_a5_pack || good === constants.GOOD.full_service_a6_pack) {
      total += 200 * quantity
    } else if (good === constants.GOOD.full_service_a4 || good === constants.GOOD.full_service_a5 || good === constants.GOOD.full_service_a6) {
      total += 100 * quantity
    } else if (good === constants.GOOD.full_service_c5 || good === constants.GOOD.full_service_c6 || good === constants.GOOD.full_service_dl) {
      total += 100 * quantity
    } else if (good === constants.GOOD.stamp_us_first_class) {
      total += 55 * quantity
    } else if (good === constants.GOOD.stamp_us_international) {
      total += 110 * quantity
    } else {
      alert(\`Unknown good: \${good}\`)
    }
  }

  let tax = 0.0625 * total
  total += tax
  total *= constants.CURRENCY.usd_to_gbp // Currency conversion

  return Math.ceil(total) // Round up
}`

let printers = [
  {
    created: new Date(),
    name: 'Scribeless',
    location: 'GB',
    serviceable_countries: [],
    serviceable_goods: [constants.GOOD.full_service_a4_pack, constants.GOOD.full_service_a5_pack, constants.GOOD.full_service_a6_pack, constants.GOOD.full_service_a4, constants.GOOD.full_service_a5, constants.GOOD.full_service_a6, constants.GOOD.full_service_c5, constants.GOOD.full_service_c6, constants.GOOD.full_service_dl, constants.GOOD.insert_light, constants.GOOD.stamp_gb_first_class, constants.GOOD.stamp_gb_second_class, constants.GOOD.stamp_gb_europe, constants.GOOD.stamp_gb_world_zone_1,
                        constants.GOOD.stamp_gb_world_zone_2, constants.GOOD.ship_to_self_gb],
    lead_time_code: lead_time_code,
    profit_code: profit_code_scribeless,
    is_fallback: true
  },
  {
    created: new Date(),
    name: 'Caddy Printing',
    location: 'US',
    serviceable_countries: ['US', 'AU', 'IO', 'CX', 'CC', 'CK', 'FJ', 'PF', 'TF', 'PF', 'CC', 'KI', 'MO', 'NR', 'NC', 'NZ', 'NU', 'NF', 'MP', 'PG', 'LA', 'PN', 'SG', 'SB', 'TK', 'TO', 'TV', 'AS', 'WS', 'AL', 'DK', 'KG', 'RU', 'AD', 'EE', 'LV', 'SM', 'AM', 'FO', 'LI', 'RS', 'AT', 'FI', 'LT', 'SK', 'AZ', 'FR', 'LU', 'SI', 'PT', 'GS', 'GE', 'MK', 'ES', 'DE', 'SE', 'BY', 'GI', 'MT', 'CH', 'BE', 'GR', 'MD', 'TJ', 'BA', 'GL', 'MC', 'TR', 'BG', 'HU', 'ME', 'TM', 'IS', 'NL', 'UA', 'IE', 'IT', 'NO', 'UZ', 'HR', 'PL', 'VA', 'CY', 'KZ', 'CZ', 'XK', 'RO', 'MX', 'CA', 'AF', 'AX', 'DZ', 'AO', 'AI', 'AQ', 'AG', 'AR', 'AW', 'BS', 'BH', 'BD', 'BB', 'BZ', 'BJ', 'BM', 'BT', 'BO', 'BW', 'BV', 'BR', 'BN', 'BF', 'BI', 'KH', 'CM', 'CV', 'KY', 'CF', 'TD', 'CL', 'CN', 'CO', 'KM', 'CG', 'CD', 'CR', 'CI', 'CU', 'DJ', 'DM', 'DO', 'EC', 'EG', 'SV', 'GQ', 'ER', 'ET', 'FK', 'GF', 'GA', 'GM', 'GH', 'GD', 'GP', 'GU', 'GT', 'GG', 'GN', 'GW', 'GY', 'HT', 'HM', 'HN', 'HK', 'IN', 'ID', 'IR', 'IQ', 'IL', 'JM', 'JP', 'JE', 'JO', 'KE', 'KP', 'KR', 'KW', 'LB', 'LS', 'LR', 'LY', 'MG', 'MW', 'MY', 'MV', 'ML', 'MH', 'MQ', 'MR', 'MU', 'YT', 'FM', 'MN', 'MS', 'MA', 'MZ', 'MM', 'NA', 'NP', 'AN', 'NI', 'NE', 'NG', 'OM', 'PK', 'PW', 'PS', 'PA', 'PY', 'PE', 'PH', 'PR', 'QA', 'RE', 'RW', 'SH', 'KN', 'LC', 'PM', 'VC', 'ST', 'SA', 'SN', 'SC', 'SL', 'SO', 'ZA', 'LK', 'SD', 'SR', 'SJ', 'SZ', 'SY', 'TW', 'TZ', 'TH', 'TL', 'TG', 'TT', 'TN', 'TC', 'UG', 'AE', 'UM', 'UY', 'VU', 'VE', 'VN', 'VG', 'VI', 'WF', 'EH', 'YE', 'ZM', 'ZW', 'PF', 'CC'],
    email: 'ken@caddyprinting.com',
    serviceable_goods: [constants.GOOD.full_service_a4_pack, constants.GOOD.full_service_a5_pack, constants.GOOD.full_service_a6_pack, constants.GOOD.full_service_a4, constants.GOOD.full_service_a5, constants.GOOD.full_service_a6, constants.GOOD.full_service_c5, constants.GOOD.full_service_c6, constants.GOOD.stamp_us_international, constants.GOOD.stamp_us_first_class, constants.GOOD.ship_to_self_us],
    lead_time_code: lead_time_code_us,
    profit_code: profit_code_caddy
  },
  {
    created: new Date(),
    name: 'Printed Word',
    location: 'GB',
    serviceable_countries: ['GB', 'IM'],
    email: '',
    serviceable_goods: [constants.GOOD.full_service_a4_pack, constants.GOOD.full_service_a5_pack, constants.GOOD.full_service_a6_pack, constants.GOOD.full_service_a4, constants.GOOD.full_service_a5, constants.GOOD.full_service_a6, constants.GOOD.full_service_c5, constants.GOOD.full_service_c6, constants.GOOD.full_service_dl, constants.GOOD.stamp_gb_first_class, constants.GOOD.ship_to_self_gb],
    lead_time_code: lead_time_code,
    profit_code: profit_code_printed_word
  }
]

module.exports.migrate = async ({firestore}) => {
  await utils.delete_collection(firestore.collection('printers'))

  let proms = []
  for (let printer of printers) {
    proms.push(firestore.collection('printers').doc().set(printer))
  }

  return Promise.all(proms)
}
