module.exports.migrate = async ({firestore}) => {
  let user_docs = await firestore.collection('users').where('membership', 'in', ['Personal', 'Startup', 'Scale']).get()
  for (let user_doc of user_docs.docs) {
  	let user = user_doc.data()
    user.id = user_doc.id

  	console.log(`User ${user.id} (Stripe customer ${user.stripe_customer_id}) is on a ${user.membership} membership. Please manually add subscription to this user.`)
  }
}
